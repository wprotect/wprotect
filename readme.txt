=== WProtect Worker ===
Contributors: wprotect
Tags: manage multiple sites, backup, security, migrate, performance, analytics, Manage WordPress, Managed WordPress, WordPress management, WordPress manager, WordPress management, site management, control multiple sites, WordPress management dashboard, administration, automate, automatic, comments, clone, dashboard, duplicate, google analytics, login, manage, wprotect, multiple, multisite, remote, seo, spam
Requires at least: 3.1
Tested up to: 4.9
Stable tag: trunk
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/quick-guide-gplv3.html

A better way to manage security and maintenance of dozens of WordPress websites.

== Description ==

So you're looking for a better way to manage WordPress websites? We have you covered! [WProtect](https://wprotect.cc/ "Security and maintenace for WordPress Websites") taken care of with a click of a button! Saves you time and nerves, so you could focus on things that matter. It is fast, secure and free to try.

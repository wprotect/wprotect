<?php
/*
 * This file is part of the ManageWP Worker plugin.
 *
 * (c) ManageWP LLC <contact@managewp.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Class MWP_Configuration_Conf
 *
 * @package src\MWP\Configuration
 */
class MWP_Configuration_Conf
{
    /**
     * @var string
     */
    protected $deactivate_text;

    /**
     * @var string
     */
    protected $master_url;

    /**
     * @var string
     */
    protected $master_cron_url;

    /**
     * @var int
     */
    protected $noti_cache_life_time = 0;

    /**
     * @var int
     */
    protected $noti_treshold_spam_comments = 0;

    /**
     * @var int
     */
    protected $noti_treshold_pending_comments = 0;

    /**
     * @var int
     */
    protected $noti_treshold_approved_comments = 0;

    /**
     * @var int
     */
    protected $noti_treshold_posts = 0;

    /**
     * @var int
     */
    protected $noti_treshold_drafts = 0;
    /**
     * @var string
     */
    protected $key_name;

    /**
     * @param array $data
     */
    public function __construct($data = array())
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (property_exists($this, $key)) {
                    $this->$key = $value;
                }
            }
        }
    }

    /**
     * Convert object to array. Much more convenient for wordpress serialization
     *
     * @return array
     */
    public function toArray()
    {
        $vars   = get_class_vars(get_class($this));
        $return = array();
        foreach ($vars as $key => $value) {
            $return[$key] = $this->$key;
        }

        return $return;
    }

    /**
     * We will use this function to notify server which fields and order to use in diff calculation
     * @return array
     */
    public function getVariables()
    {
        $vars = get_class_vars(get_class($this));

        return $vars;
    }

    /**
     * @param string $key_name
     */
    public function setKeyName($key_name)
    {
        $this->key_name = $key_name;
    }

    /**
     * @return string
     */
    public function getKeyName()
    {
        return $this->key_name;
    }

    /**
     * @param string $deactivate_text
     */
    public function setDeactivateText($deactivate_text)
    {
        $this->deactivate_text = $deactivate_text;
    }

    /**
     * @return string
     */
    public function getDeactivateText()
    {
        return $this->deactivate_text;
    }

    /**
     * @return string
     */
    public function getNetworkNotice()
    {
        return $this->getNoticeHtml('Now just send your <strong>encryption key</strong> to WProtect, to enable backups, uptime monitoring, website cleanup and a lot more! The <strong>connection key</strong> can found on the plugin page, in the Worker description, to verify that you are the website administrator. If you have any problems, please <a href="mailto:support@wprotect.cc?subject=Problems finding Connection Key">email support</a>.<br/><div class="connection-key-screenshot">&nbsp;</div>');
    }

    /**
     * @param mixed $master_cron_url
     */
    public function setMasterCronUrl($master_cron_url)
    {
        $this->master_cron_url = $master_cron_url;
    }

    /**
     * @return mixed
     */
    public function getMasterCronUrl()
    {
        return $this->master_cron_url;
    }

    /**
     * @param mixed $master_url
     */
    public function setMasterUrl($master_url)
    {
        $this->master_url = $master_url;
    }

    /**
     * @return mixed
     */
    public function getMasterUrl()
    {
        return $this->master_url;
    }

    /**
     * @param mixed $noti_cache_life_time
     */
    public function setNotiCacheLifeTime($noti_cache_life_time)
    {
        $this->noti_cache_life_time = $noti_cache_life_time;
    }

    /**
     * @return mixed
     */
    public function getNotiCacheLifeTime()
    {
        return $this->noti_cache_life_time;
    }

    /**
     * @param mixed $noti_treshold_approved_comments
     */
    public function setNotiTresholdApprovedComments($noti_treshold_approved_comments)
    {
        $this->noti_treshold_approved_comments = $noti_treshold_approved_comments;
    }

    /**
     * @return mixed
     */
    public function getNotiTresholdApprovedComments()
    {
        return $this->noti_treshold_approved_comments;
    }

    /**
     * @param mixed $noti_treshold_drafts
     */
    public function setNotiTresholdDrafts($noti_treshold_drafts)
    {
        $this->noti_treshold_drafts = $noti_treshold_drafts;
    }

    /**
     * @return mixed
     */
    public function getNotiTresholdDrafts()
    {
        return $this->noti_treshold_drafts;
    }

    /**
     * @param mixed $noti_treshold_pending_comments
     */
    public function setNotiTresholdPendingComments($noti_treshold_pending_comments)
    {
        $this->noti_treshold_pending_comments = $noti_treshold_pending_comments;
    }

    /**
     * @return mixed
     */
    public function getNotiTresholdPendingComments()
    {
        return $this->noti_treshold_pending_comments;
    }

    /**
     * @param mixed $noti_treshold_posts
     */
    public function setNotiTresholdPosts($noti_treshold_posts)
    {
        $this->noti_treshold_posts = $noti_treshold_posts;
    }

    /**
     * @return mixed
     */
    public function getNotiTresholdPosts()
    {
        return $this->noti_treshold_posts;
    }

    /**
     * @param mixed $noti_treshold_spam_comments
     */
    public function setNotiTresholdSpamComments($noti_treshold_spam_comments)
    {
        $this->noti_treshold_spam_comments = $noti_treshold_spam_comments;
    }

    /**
     * @return mixed
     */
    public function getNotiTresholdSpamComments()
    {
        return $this->noti_treshold_spam_comments;
    }

    public function getNotice()
    {
        return $this->getNoticeHtml($this->getDefaultNoticeText());
    }

    private function getDefaultNoticeText()
    {
        return 'Now just send your <strong>encryption key</strong> to WProtect, to enable backups, uptime monitoring, website cleanup and a lot more! The <strong>connection key</strong> can found on the plugin page, in the Worker description, to verify that you are the website administrator. If you have any problems, please <a href="mailto:support@wprotect.cc?subject=Problems finding Connection Key">email support</a>.<br/><div class="connection-key-screenshot">&nbsp;</div>';
    }

    private function getNoticeHtml($message)
    {
        return <<<HTML
<div class="updated" style="padding: 0; margin: 0; border: none; background: none;">
    <style scoped type="text/css">
        .mwp-notice-container {
            box-shadow: 1px 1px 1px #d9d9d9;
            max-width: 100%;
            border-radius: 4px;
        }

        .mwp-notice {
            width: 100%;
            max-width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            min-height: 116px;
            margin: 15px 0;
        }

        .connection-key-screenshot {
            width: 100%;
            height: 84px;
            background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABEEAAABRCAYAAAA9+LhcAAAKo2lDQ1BJQ0MgUHJvZmlsZQAASImVlwdQFFkax1/35EQaGMkMOSOZASTHIedkYpiBYQjDMDComJVFBdeAiggogixRwVWJa0BEMbAIBjAvyCKirosBEyrbwDHc3tXd1f2rvupfff36/75+/V7V1wCQr7IEghRYCoBUfqYwxMuVHhUdQ8c9BRCAAQXQgRyLnSFwCQryA4jmr3/XhwFkNKLbxjNe/37/v0qaE5/BBgAKQjiOk8FORfgMEifZAmEmACgOktdclSmY4e0IywqRAhEunWHuHJ+c4bg57podExbihvA9APBkFkvIBYD0O5KnZ7G5iA8ZjbApn8PjI2yJsCM7kYXMQ0buAaPU1LQZPoqwXtw/+XD/5hkn9mSxuGKee5dZ4d15GYIU1pr/czn+t1JTRPNzaCBBThR6h8zMh6xZTXKar5j5cQGB88zjzNU0w4ki7/B5Zme4xcwzh+XuO8+i5HCXeWYJF57lZTLD5lmYFiL256cE+In945lijs/wCJ3nBJ4nc56zE8Mi5zmLFxEwzxnJob4LY9zEeaEoRFxzgtBT/I6pGQu1sVkLc2Umhnkv1BAlrocT7+4hzvPDxeMFma5iT0FK0EL9KV7ifEZWqPjZTGSDzXMSyydowSdIvD6AB/wBC7Az41fP7CvgliZYI+RxEzPpLsgpiacz+WwTI7q5qRkDgJkzN/dJ39FmzxJEu76QS+8AwDYPSXIXcixNANqeAUD9sJDTfItshz0AnOtji4RZc7mZrQ4wgAgkgSxQAKpAE+gBY2AOrIE9cAYewAcEgjAQDVYANkgEqUAIVoF1YDPIBflgDzgAikEZOAZqwAlwCrSAs+AiuAJugD5wFzwEQ2AUvAQT4AOYgiAIB1EgKqQAqUHakCFkDjEgR8gD8oNCoGgoFuJCfEgErYO2QvlQAVQMlUO10M9QG3QRugb1Q/ehYWgcegt9gVEwGZaFVWAdeDHMgF1gXzgMXg5z4XQ4G86Bd8FFcAV8HG6GL8I34LvwEPwSnkQBFAlFQ6mjjFEMlBsqEBWDSkAJURtQeahCVAWqAdWO6kbdRg2hXqE+o7FoKpqONkbbo73R4Wg2Oh29Ab0TXYyuQTeju9C30cPoCfR3DAWjjDHE2GGYmCgMF7MKk4spxFRhmjCXMXcxo5gPWCyWhtXF2mC9sdHYJOxa7E7sYWwjtgPbjx3BTuJwOAWcIc4BF4hj4TJxubhDuOO4C7hbuFHcJzwJr4Y3x3viY/B8/BZ8Ib4Ofx5/Cz+GnyJIEbQJdoRAAoewhrCbUEloJ9wkjBKmiNJEXaIDMYyYRNxMLCI2EC8THxHfkUgkDZItKZjEI20iFZFOkq6ShkmfyTJkA7IbeRlZRN5FriZ3kO+T31EoFB2KMyWGkknZRamlXKI8oXySoEqYSDAlOBIbJUokmiVuSbyWJEhqS7pIrpDMliyUPC15U/KVFEFKR8pNiiW1QapEqk1qUGpSmiptJh0onSq9U7pO+pr0cxmcjI6MhwxHJkfmmMwlmREqiqpJdaOyqVupldTL1FFZrKyuLFM2STZf9oRsr+yEnIycpVyE3Gq5ErlzckM0FE2HxqSl0HbTTtEGaF8WqSxyWRS/aMeihkW3Fn2UV5J3lo+Xz5NvlL8r/0WBruChkKywV6FF4bEiWtFAMVhxleIRxcuKr5RkleyV2Ep5SqeUHijDygbKIcprlY8p9yhPqqiqeKkIVA6pXFJ5pUpTdVZNUt2vel51XI2q5qjGU9uvdkHtBV2O7kJPoRfRu+gT6srq3uoi9XL1XvUpDV2NcI0tGo0ajzWJmgzNBM39mp2aE1pqWv5a67TqtR5oE7QZ2onaB7W7tT/q6OpE6mzTadF5riuvy9TN1q3XfaRH0XPSS9er0Lujj9Vn6CfrH9bvM4ANrAwSDUoMbhrChtaGPMPDhv1GGCNbI75RhdGgMdnYxTjLuN542IRm4meyxaTF5PVircUxi/cu7l783dTKNMW00vShmYyZj9kWs3azt+YG5mzzEvM7FhQLT4uNFq0WbywNLeMtj1jes6Ja+Vtts+q0+mZtYy20brAet9GyibUptRlkyDKCGDsZV20xtq62G23P2n62s7bLtDtl96e9sX2yfZ398yW6S+KXVC4ZcdBwYDmUOww50h1jHY86DjmpO7GcKpyeOms6c5yrnMdc9F2SXI67vHY1dRW6Nrl+dLNzW+/W4Y5y93LPc+/1kPEI9yj2eOKp4cn1rPec8LLyWuvV4Y3x9vXe6z3IVGGymbXMCR8bn/U+Xb5k31DfYt+nfgZ+Qr92f9jfx3+f/6MA7QB+QEsgCGQG7gt8HKQblB70SzA2OCi4JPhZiFnIupDuUGroytC60A9hrmG7wx6G64WLwjsjJCOWRdRGfIx0jyyIHIpaHLU+6ka0YjQvujUGFxMRUxUzudRj6YGlo8usluUuG1iuu3z18msrFFekrDi3UnIla+XpWExsZGxd7FdWIKuCNRnHjCuNm2C7sQ+yX3KcOfs54/EO8QXxYwkOCQUJz7kO3H3c8USnxMLEVzw3XjHvTZJ3UlnSx+TA5Ork6ZTIlMZUfGpsahtfhp/M70pTTVud1i8wFOQKhtLt0g+kTwh9hVUZUMbyjNZMWaS56RHpiX4QDWc5ZpVkfVoVser0aunV/NU9awzW7Fgzlu2Z/dNa9Fr22s516us2rxte77K+fAO0IW5D50bNjTkbRzd5barZTNycvPnXLaZbCra83xq5tT1HJWdTzsgPXj/U50rkCnMHt9lvK9uO3s7b3rvDYsehHd/zOHnX803zC/O/7mTvvP6j2Y9FP07vStjVu9t695E92D38PQN7nfbWFEgXZBeM7PPf17yfvj9v//sDKw9cK7QsLDtIPCg6OFTkV9R6SOvQnkNfixOL75a4ljSWKpfuKP14mHP41hHnIw1lKmX5ZV+O8o7eK/cqb67QqSg8hj2WdexZZURl90+Mn2qrFKvyq75V86uHakJqumptamvrlOt218P1ovrx48uO951wP9HaYNxQ3khrzD8JTopOvvg59ueBU76nOk8zTjec0T5T2kRtymuGmtc0T7Qktgy1Rrf2t/m0dbbbtzf9YvJL9Vn1syXn5M7tPk88n3N++kL2hckOQceri9yLI50rOx9eirp0pyu4q/ey7+WrVzyvXOp26b5w1eHq2Wt219quM6633LC+0dxj1dP0q9WvTb3Wvc03bW629tn2tfcv6T9/y+nWxdvut6/cYd65cTfgbv9A+MC9wWWDQ/c4957fT7n/5kHWg6mHmx5hHuU9lnpc+ET5ScVv+r81DlkPnRt2H+55Gvr04Qh75OXvGb9/Hc15RnlWOKY2Vvvc/PnZcc/xvhdLX4y+FLycepX7h/Qfpa/1Xp/50/nPnomoidE3wjfTb3e+U3hX/d7yfedk0OSTD6kfpj7mfVL4VPOZ8bn7S+SXsalVX3Ffi77pf2v/7vv90XTq9LSAJWTNtgIoJOCEBADeVgNAiUZ6hz4AiBJzPfGsoLk+fpbAf+K5vnlW1gBUOwMQvgkAP6RHOYKENsJk5DrTEoU5A9jCQhz/UEaChfmcFxnpLDGfpqffqQCAawfgm3B6eurw9PS3SqTY+wB0pM/14jPCIn8oR2VmqEc1bQL8i/4CcwoCId5e1pEAAAIEaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDxleGlmOlBpeGVsWURpbWVuc2lvbj44MTwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj4xMDg5PC9leGlmOlBpeGVsWERpbWVuc2lvbj4KICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CnaHnVAAAEAASURBVHgB7Z0LfM/1/sdfbGaFltbBHH+mmsuxtWMqRDW5HiS3yv0SFYeKSHTR0gWVyEkUDkUuJyQhuURSnGSdNc6Y2zjLWC2WyW9z+f1fn8/3+/39vr/ffr/fNiZN788ev31vn+vzc/l+v+/v+/P+lMrOznbCyzmdTpw7dw4OhwO3rv/R4+r+njd7HMuBEBACQkAICAEhIASEgBAQAkJACAgBISAESgKB4KJmMuPIkaIGEf9CQAgIASEgBISAEBACQkAICAEhIASEgBC47ASKLASJqFr1smdaMiAEhIAQEAJCQAgIASEgBISAEBACQkAICIGiEihd1ADiXwgIASEgBISAEBACQkAICAEhIASEgBAQAiWRgAhBSmKtSZ6FgBAQAkJACAgBISAEhIAQEAJCQAgIgSITECFIkZFJACEgBISAEBACQkAICAEhIASEgBAQAkKgJBIQIUhJrDXJsxAQAkJACAgBISAEhIAQEAJCQAgIASFQZAIiBCkyMgkgBISAEBACQkAICAEhIASEgBAQAkJACJREAiIEKYm1JnkWAkJACAgBISAEhIAQEAJCQAgIASEgBIpMQIQgRUYmAYSAEBACQkAICAEhIASEgBAQAkJACAiBkkhAhCAlsdYkz0JACAgBISAEhIAQEAJCQAgIASEgBIRAkQmIEKTIyCSAEBACQkAICAEhIASEgBAQAkJACAgBIVASCYgQpCTWmuRZCAgBISAEhIAQEAJCQAgIASEgBISAECgyARGCFBmZBBACQkAICAEhIASEgBAQAkJACAgBISAESiKB4EuV6Q0bNl1U1M2bx19UeAksBISAEBACQkAICAEhIASEgBAQAkJACAgBO4FLJgRRiTSMi0GpUqX0z56ov32n0wn1+3disj8vcl4ICAEhIASEgBAQAkLgd0Ag73QeQq4K+Q1zkoe8vBCE/JZJFrp0v+e8FboQLo/FXrd55MOK+11WnVXqkpBHK6+yNQlcWf1OqvW3I3BJp8OEhobi6quv1r9y5coh0M/yp8JctHOcwLH0o8jO8Ywp++hRHPvJ4Xkyx/R71jjt+Il+lD/rp+LxCuIZwRV2dBHsLoTEsU2z0a3bk1iz748E+UJISRghIASEwOUjkHMoEWvX7UKeLQt56YlYuWIbcs7ZTmbvxdoVm5Fh92i77HP33M/YumY94zd/3N+6K8OnVzlZ3ATysHbUnXhkxaEiR5y2ZhSatWiBL36zqlJ5bYFmzSbDd5K8nnBhZSly4fMFKChv+QL8rk/kHVql63Z8cVVu3iGMbMa6m7jtty+3SrvJnVh5oIBBySOPF9iWzLSWFpRWkSmo9nW52naRM/sbBriy+t1vCC5/UqcPYenEUWjCvlL87Td/cr+HM5dUCBIcHIwyZcpQYh+it2rf38/yo8JcrMveMgmRkVVRa+Rqd1QntqJKtaqo+X+9sNv1vu3A4m43ar8Ld6mTDixqXQeR9Of6MZ4q5Utj4soUd1wXuvfTd5g48knMWH/0QmPwHa4Y471wdr6zVtDZ4+mf4uMlk3A0tyCfcl0ICAEhIAQuG4GsRLyQMBjJto8LOz4chvF8aNqU7n65SFs3AS9M3AQEFSGneRl498VxeOHtt7Dwg4V4i/sjH34ATSau9xC6FCFG5B1YjCadPrrg8B5p5e1F/yYDkOoupsflkn5wMhPIyCl64a6v1QrtOwxBjXCDQNqSAei/YO8lxBGCWi0Hov3AxtBJ+qiXk2kXVpaiZzoPS/vdiYW7LW5eeSt6hL+rECHh0Xi4ZTvE32hU7kX3p5BwtOrdDkOb1rgM5cxDFlM9SS2PgM4rjxfWloy08gpKK2BGfF+80H7qO7Yr5ax3v/Pul1dKOS99OdJWTcAbK7Iw5tUpuL3S71pfq9hgXFIhiJoKU7p0aT0dRm0D/ex+L7Z0Ybe0RZtSpXFq8Xc4Zmp4ZO/5WkfrPLMM2/abUpCzB7Fp7UmUKjsA8bUNDZSrospof4/PmI9FS+Zj8ujO+jihYz0s1oKSi8jdTylImDIJn6Udv4hIfAQtxngvhp2PnBXiVMVC+BEvQkAICAEhcDkJlK97F2oxAztTLSlIBrYuMXK0Kcn6Lp+HnRv4Ety1PSJMIUhOVgbSDhxCWoYVzkcpgtQDVxTmLF6OOXNnY8VXm7HiH0OAFePwiflFNSfLCJ+VzrgO/eyKJM+KP90W/zm+iGScBDKzcIQqofb3kYD5OZeDDJVX/uwygRzGo/4yMn9Gjv2CmYu8bJ4/7cqS3slR52zvXDrfjDcjy3aSIhpVLo8zOTnuNE5zX8V7+medpyyvNPLsflWq9J9lT1SFtx0HKnu4ml/C8uu6srPUpQFyMox6tOe//A0tMOKxTohU1ZdHdj+R0rEspsky2bSDXGW3VZEZrbnJz0FzscVhcYq8uxtG9Gykp1T4qxddFuanoHaXj59HfRh5Uhm02lhGtrum8nIykJVNgQvbn8XYnjdVF/a6c4U169JXfwhUPy5eLFeWygfbuG6rVl2p40Oqb2R4tCcdzkrT+5qVR4tVurtfoXwNdB85HI2rqXbhvz9ZbNKsfJgZVe1ftQHjuspTebR6cDi6NIowfARK24wjL9uzzVljgHk58MZVJtXo8k/BceXbY1zyyiNDmiIg9lOjPO5E87dZ9zWmqPoT24hqg666t3nwnb7pwV998bKRH8Ofar8Z5rioxyB389T9MctSh1f1Z7Zdqy/ahgUzUbYfNbYyv97jjOnBzyZwOCs9D8xWitbY7XExP1ePfmq1G9uYaO93+fql6i8mI1cBdB/yOxj5HOussK7y2MaCgq5Z7TbHH1/mR/dl1T9tY54Vr8+txSFQv2dAV37txdXld48hakzPU3n4iffxlt0RHxeNiPJmqrZ7one7sMql0zDvyZ7n3GORVfZ87S7HuLd5j1uu8cNrDLCzCNiH/MRrD6/2L17twjtGH8dKwFGQU36UPZBicdfWRevWFbBm7TKknHgGla8Hdn+50RX1mq0p6FevPnA0BXPPn8XV9zRDpG0WjhKK9OvXA3UUnY490LhKVzQatgyLGO6BenXx8bNdMf4/dTGoYxV8PPgpbO7yPn5Y1AOh1Mh489nnMHqWoYES2/VFvDPlGcRWAY6tn4Z72zym87Bu+pPotjwC/WbMRJtqPKU0OYYORMKS7/T1NsNmYvJLA9x5OnsUiyc8h34Js/X1kDYD8O4LL+KBBlUCx6t9F/HfhbI7ewIfT2G5Rht5DIodgNnvGnlUOUhb/hyiX/wOLw/uhIzlwzF14wNIzJqZL3OO3cvQr+fLWLE7DluPzUQsO2LSopfxSK+xSIITpYLb4u01/0C/+Jo6rL9469jqM18ickIICAEhIASKRuCqGmgfBbyXdAh94+rx7W8XluI6tGoCrF2WiLwONRBCjY5N3wN9H6rFuPOwY87jeGzWLlc6tXq/incGGS+wrpO2HfsDYHhsI0RjGh/O+LB2ei8e7TBYyUmQqhUNBuKzr7ohmfGPtMWPJsOxYnwnnFw8GD2naY/o2fZ91BoyG3N61AiYn7yMRDzadRh22vLzxD+W455r16F172n67NMPdAQqDcfGjzrZ7Brk4ZN+HfFG7Ev4ilMxtMtJROu2w9D33dV4OOok3nv8AbxLLpZrPHwKXu8ap8vVmuWaun4zGlxlXE37sC36507HV4PqIXXR4+g/S503yvIw4+tbz3o65X11RS/0n9YdG796gPnJw8on2mL894pNH75yUk2cxy/UmoKvhkcHLHtZpp06uTeaTLZyyJetri9hyfA7dbxrJ96PF1a4X5Br9Z6COYPikLd7MZoNWIh31v8Le3q0xRvUKAFGoTWFY0Nnr0f3qCy8N9Sz7H1fXYyHm5gvwlZyVMPu0WEAujBM3zosyYGPyHwy+v6D/OJY3vT1aP3AOExdvRkVVt2vy/zJPOCefPXSDr7K0vipeXid7dPb2Vnra2xnrvog8x6sm8Zd62HlEncbfnjKYvSNzcEjrakZpAKN6s1+QAHeRj77LDHy9hnr44iquxUhCM/cpbUQlNe+Q/pg5bT3XcfRQ6bjnR7sS0XoK3kHlqHDgGXsGz+72mrj3kNQ4bNpWKv5M7qbh+Cz6Q+wDbCbfvU+uo7SjYhHyjXC1BWvogHfpnX78spjeNdXWe/so2TRlWXsMns17kp83Gd/2uqn/4UHGX3irbDr2HRVu7lOt5FkTp16ayDbdn+zbftLmyHS1k1Gz4SPVIZtLorxzEa02VdsFzx28w6txyM9xhn1o65UYj7o4vX/PHwxeTCeXmL0KX2qw1hsfKqFbusLOWXHyCMHG8txXHu0Q29EvrIYz99ltN2cxPfR+tH3MfUz9l13l9QhlEzwjREtzP5gRNLllXl44i7VBgOlz/razvoa5lVfq1lfYUY81v+c3avQesBEhA+cghX9o7GwbUfkqvHGHB+MdgI9FoTsXcQ2sxVdmnDM/sqKoR5eX/ImGkcwt+x/41v0xkrrErd92c4fvtWrn9qu691A4c5lBOj7+e8Neux+tRPC7X2wkGNi4yRjHPxsYxwe9eqX7/6zPR5+cDKeX7IZrczi7PhnWzw2zxon7YXimOlnrAPLs5Bj2Vu+xnFOgXrjAY4DVv9jlO0T5mFMS9b36V16HIm4+Trs/N4aQ6PwypLpuIvsVVvtyraqNJUMF4XXV8xGY7u0y7pk26YuYpm/aYzG36/CVut81EAsmd3H+AARgL/RNmbpjxp6DBs4FU+seMxsr+PQeh3wsBq/w9Q9cZRrnFHJqHtilzj2J5Yr3z15fQN9LuRmfiyxODXpg4evXol311llr8dxfLpuz/n7eDt8sPEpCtWN8WNhVD1kfeUee933jgDth3nM14cUl3fJRXVMb5edne30/p04ccKZlZXl/OGHH5xV3/uPx+/02fPOwvxWfva50+FwOM+dO0fZRuGc8qvCqLCFSSOQn21TOjtZVmfCVz8zrp+dE4KucqJ0fWcsSjlD28x0Hmc50pY/o/30m7/fTO9X59yu4c5SZTo7v8txlzNtyQjt794Zu+jP8KPitn4VBs53Hj+xw3lvqSB9jkIKZ782V+t9vrQ7t/2k0nrR5V/ng+cX7f7VedoWrs3AEc5+Xetrf0FhrzmPatY/O6e3udYV1+MD27riWZQWIN5C1pMvhkVn96vfPE7+t+J/3vnd3AGufCtuFDRpxtb5uTvJ+6f1TmrwaH+ucDOMelQcHx89QNefrtf1geP1VS45527TwqKILHLPmGNEEcNdRD+UOhLWv7c2kLLsEWdcw7nObLbrAytHOuPuWeo8kLaW5/o7t//K+tr/Cfc7OLecOO/8MWku95s6P0z6Sfed9CR1ralzRtIv+fvSr3uc3RhHUq67zrN1vE2d/9xJ/7zeh2HjnmV6JxzObPr78Rsj/hlfHNTx/Zi6iXE0dfZZtMc41unPdf5oxhk4P784/3kP4+/1rjOFeT+d+4vz45d53HC884Dqwye+1fnbkuXIn3deT986iX4fcaYoBorNhvE8TnAeYNrbZ/bX1z7fb5Q7ZcO7PG7qnG+WS5V7+0l3uVPe7+9sOdMoQ8oiMibP+d/84Mz+NX/a2alLddxJKl2LkeKfpeL7wTma6bz5zS8F1IXD+eFDqqwdnB/vNOoq5QuTLcOe/p+q36bOj/cb6R/Yqq4l6LIa6bPezPyrsrZ861sXI6PsCc7tx1RYh3P7sgSG7a/bh2fbZh56NXV2e98qt2pnTV1xHfhUhWNdkm3KIvJku1NtMH+9WGUZ6dzyPzPNRSrsI07NSIWx/eys9XmzHer64P4Q5iHunkmuNrHmLaMujfIabebNbwxmKrw9bymLVBnINFXVu2pPHXSZPlT1Thafv8bjexbqMgVum555Npg3db78qdHuk1aqttbUOXqZwe7AF6p9mW2K7bYlr/WZ+bVO5/SJg843yTmu11KTpdG+PrTq/VMVlxnWbE+6/7FsRh5t/Slg/7PqIcG5Je0Xtl1VBodzvkcdB0g7y51vVc/ZWXucY1X/VPVo6yv2unTv/+KcofvyXKPefv3JxV6X5UQy67W/a1z6MdVs32lGe3Hn0TO/W15j+g99YrQ75mnNszx+Yq1He7LakNFuxjuT1HjBseTzmaotmO0+UPp6nGE/eGuTMW6pcqs2+OzXLn59lh10Zu838tzn/WQzfeaVZZ6h25bRXrJTFzLNhTq/Vpvp9tpaI16vdmD0L6NPq3raovLLsuk+xrK62XruBwoXqO9b7f3NDdbYbZRn9IYf9DhW1DHR3u9UX1Njubtf/uR8WfWBRUZaalxUTId8yrS8yxZgrDPK09+5xhrHzX635pjqGxyH7xnv3K7HXY4DK9WYY4z/1rjc8tlPnOnqXkT2Kn1jjHc4PzbbkWad+4NzBsfiIazjfHnzyqtxb3CPA6odq/tfN/P+F4i/1R5GL/rW+aO6r5j3SBXGPX6b/egh9iPV59iOPcY/635juye7yvoy2xnzq/Kkxp+4Jz7R7S47bZM+Np4BfnHOf6gDxy2zDZ88qO9Xo3W9WOOHYqr6pXk/9hovfbYfsw+pePXYcSxZx9typtVXbG2Yfi/pdBi+rF42V7vZ33Ta27YfpEpaCkafO41+78/COzP+htz1H2HPWUqa/2NobLS5xdAqsGc2V82YoR/H0e8wd9ZMfalieU/1gsfn7wIbKjJn9MDRlW/jY+c5xI5ej+yVMzF9ZQ4+TWgL59nVGE97IpXbP4PTO+freO7953xsc6zEvTeFIs0M12/uLnw04zVMX7QDn46uj3PZNBaqjJfsW43Ba06gTOxrOJizEhNmrKRWxYs6njVb/MerPdj/qbLkODx+9sv2/SKz27dM5zE49kX818rjGiOPz7y2mpZW3C522BIcdZzHr6dmwq6tcTz9cyRUboc1zvOYsP4IBjW4lvxT8ObfV+jpSoknWPaXZmJb2iegIAuvzihcvO6UZU8IXDiBMhwjysTVQJnbb0CZZrUR/LebEdzpNgR3vxPB/VohaNC9COLX3qBR/RD03CAEvTwcpSeNQel/vIDS776K0u+9idKL3kXpZe+h1Op/odQGtusvP0Opb75AqaR/o9TuJJTav5tfPNOoGXYUyD7ODvsrcL6wupEXXjYJKQQKS6BqXBd6/QJp1M5I/ngbat0Xh4hq9dCKX8130C5IRuoX/OraHbX5ZfRIEvfRgmq1J5G2+xBOlo+gP2DpN4d8JhfCOBbOfB/vzXkf706mwU1++UelgWhVx/jMSn0QDO3TjvFRtT2I8e9WaQ1H9yY1dHzlb7gTIwbya9sHiVCavyFXleX/stqv8hAwP6cz9Ff0oWP7IFIlF0S1eNoUW7JkCPTHw5AKKgqUVSruPlx4bHt+md+FtTtVyvxKtWgVwnurKUF52LNiLzVRRqNxDaMckXd1w9BK1KjZ6puDKqfb8ftgpb64Jy7CUK93X9B7ITXi0JjpJlOLOe/AVuys1ILaOj9jbSrzkcX8UEPh9rrlA5edMeWqaTZdx5K18cU8skk3PMEP4Uup9YMK12kV/IWLPsIO1mOF2D746quxxhQYr/yU18gNVnzwwp4v+LW9SQOUJ9+0AxkoXy2GIfZi5yHFye5CEN2tEVI/3MmTedj6wS7aj2iBrHlf6LpMpcHccNoBMQjawvmoF1WW8CEPoYH+7Ed7AbeqNKnu7WMo9WRti9fcPcnt0PFDXG3irl5DyGIXtur8G1Msyl7tu02oNFXdxd+gcl0etW5kS2J7NdpzCBrc253TtdREjQLappkXz009dNZaBUBk3Vq8FIXubQzNhYhbjWlryn9e+l5+YY7S04c0O05x6ffkQFYBnyP11Cqjfbnq/a7O/Dq8F8mHDDJ2Pvn6UwH9T9VDreF90aBaeT8r+fhPOy/DnW/FJyQsCo8+3UcVqWDHtraJXIeO7WbUWwi11QaNdocrTw2Ir2Yjvkqunvpx5IQxxcRjGpnbt2sv5t7hwPczDZtI/PK/gF/L+7LN+nKq3fR9eghqhTH3HEsa93xCc92ppgwFSF/VVyrra8yDd6I8xziw3GM+W07NnDidjNJy2jnxcTSj5kDj4ZYWkZEDe10ZZ7z/M64hLYx42Q562NpBhfCq9LweC5esR+qhLMT0p7bOq0ozJrDzHy5w39djMftCP7MNq2l1G1csxojbTFUNr2Q9y2a0G39joupRKt/ufnkdOg+vh52TjbEk78A2arxch+6NfaTld6wzxvFwjuN3WeN4m6ewYslyNOarSvl6nfDVkuGIOGlOJ0IF5oDTisyMq033Pi0RrupUTTMbHoWsL3ZyhKCmWBWOuV8txNJ1iUjLrIC+0zf71FpjSC+nOAzBUKVtQqenJvL+l6rvf4H5GxFFoW+HOJRX9zSVLzqP8dvqR0+yHymNHLbju/o+YRv/9AjncU9Wcaiy9r2P7Zfb8tWi9P1z6IMtdbsLqdYIfXnvM9Irjy7Tl2MMNav0tEW2uwq8lvajcW8wxo9uiFZ9iLHF38fpsZmbcITjSqD2Y/Qh6HHnCKd2HTldHg2a8Ha4Yqe+lzAyl1N+f5PpMK4Uf8Od0BtvBzUzsI6CgqRoPiHQxd9SH7VP/Y2CiUfx3Y7v4Nh2AKWvHoHYSH3Z9U/ZDWl0vad8SL3gJ3StST/GKz21RdC9Y11XmGPpRhqDut/uOhfftQeQsJoPgOpaXbcwwIhC+8s2w819sBeOrzQ65cfmtJi0oyeQfZURb9vhbVHZrC0tUHE845rM5IrOtePKgmtn99z2qD9otetY7UxO+hWD6nkKdtT5orLL3mHksd3gzoi08hjfGf1Kv4B5nx1ENgUwlhvUuy3CfLS64W1aaC+xE3bg8fgqhneu3HOUQhFn7mxOTzqOCPaNshkr9bSYIBWvFSm3/uK1eZFdIXBhBM7x6VlN6TtLJaUcNmb+SkE/Qbri49VL58ow9lDepcqGwHkV+2vZsjxW21A4Q3l34lbrgJv7Tn1snlN+1ZOT8qv2Q9S+OsfrIeaW153Wvt4qf/zRrpI4IWAnEBJhCjySEpFKddcuo9U9KwStOgDvfrENEYeUYOQh/QAUQiOD6sH6sR7rXVGEUy29cbh+HXOds+8cOZqhm2je6XA8/NSr6NKukY7L5Ucb9zAezbU8ooqxb10vX8HIj+dZ42rA/FBYoZxHOD70Rfh4TjZi8/ofEoV+ZDByRSoeji6rp74MfSba8EQ19hCPIoegglJtZ3+2nBLqWM591joTYBtUQ09HWpqyF7V/XoZa/d9Ev7AcdP18J7rn7eBDcnvUYpc/Uoi6qPV/qr4sRxsClF+Et2Ju+NL2zj/GYtLUcXhshTEtKLr3S5g0SE2V8XQGRetciB568NVE9HSp4FOgUikK4T6W1I2MbcWH3LVIzahBgVQ9PN+rLzCvN5LTO2MHw3cfZDzsW7EH2tpLAlP44Z1XV3gOdYHcyV9VqczQ5Gi0MCOEZ3kDxaKu+fcdsG36jNYm1LGEO9bW7l93knAr98aV8uoFzVUi45ztv1FS/3m1vBaq/9mN8VgBA2xdaevIPe14hFRStVpwvqwXugq2PoWrIiikNF3OLrzQejAFhEVzITc0QhdMxtqkHMSEf8EpAo0wRs3X9uFULsPDbS0u76SemtNK+Q2Uvo9yo/x1hiDWVXZzWoFPFLY08+VrLw3D8qTpJcTWDsrH9cHUp4C3Jo7jVDMjYF9O53jYfMHOF5V5wn+4iIB9XxdTv9y6Yw4Jp6BXHZrluuAx0R2FK/LIZhxLKFhPzu6D8C/eo2B2KGJ8VV35OD9jXSMKpMDpFJ58wyMMoXHOLk7fe3iyKz1jp56F2ji03bv0CfMFpkHf6XgeM2l0fBje0hc49i14Da1MYYsR2Nd/5iXzpMZl5ao8GaoKVn/qsa+gsddbMOzRpMz+49GP1D1Rp2DLj3e5eMkwCqxyxXFKe3U3PHcaOVg5itM3XfcGI05XP1WHHuOHEZ86Haj9qCW4lRv/aG+9Vf/Uc0etWK9xUF2gXx+vo+rKFeBCa6JTl2u58kgvPLI3WAs7Gkby3eGsIRwZ+vfncC4pG9cO+5vrxd1e6nuHPePSVKgS3RbduzbO9/Juv3eeNgUQqt15u0MOL+kE3z8sd9yRoXdjH+yMeGqG5PIovmknhFJkUrEC/5/0CmsF9FVztngtb9a2YuTfcO/ACErGKbaky04H6oT7CVBEdseswvuIzul0eL4u+sq3lUluv3/uZWzqtwTx1/PAcRoZtBOj7IB0uqUxmSjXDG0G85KDXdEmXLmCW7Iutfy7jASCgnAm8RCNw/EpU6mInWEvVdu8XJQyt/qY7VVfU/1dX+Mxt6odG/55nvulcs1zecaxcU7t86fi47ZULm8VDvVjmmcofDnDxn5SCV9+9QBxSYUvIUr4wg6rhS8c7ZTgxEP4wtFOnzO2nsIX+lXX+MsvfFHnjfg8hS88r4QvLKW43ymBoAjEd7gOTw8bxQy2w8PKaCJdDFfsSH30WbzA/Sf6GC+reXn8UoUhpr0K5YuODzV52giqcWj/n6e+gD73FGrZX17sHrz29fPR91keD4FZ/+P84bBWNp+5rusB88NVRpTLOuF+WEPOIexIyUPtW6NMQUwWcn29aJqpNVBfqnovwlrasMjCQNyu2TA+PuxaxgsNrznarkm4yqaOby+/APKreTXjauqXzMsdZqSF2MS0a4cXpszEP2i0tcuCGtSUYcRPz8RbFIw07t9NP4gHLLv5mJq68xC1QdyCBvVVTtWgMooZHtsCr89toffTtryPnk8/i6/v3Yx4r/yp1pCVe9J1NvcoH0BpA2IFbUBYLu80v3/6EIKAArb2GId338lC6s3d+axSA/Etr8P4qdOYj0b4wGxrVjzubeB6cfvLv6fz+yXL3cPQokDmIbcdifzeOQYf0vPj4zVVDlf0c9JzSPYVqsBzgeunwOD+PehOkoostrNI09fJ9DT//nlF9QD/zt6f6KvA/uc/Jl9XPNPehQw2pUhTonVk+1YGUcQLcGYfNTQ7TP+Z1D5isHj+MrYupQCkEeZ89ipqqRdhtcJQswHcKchFoPOQKPT8eBVqhc9iX5+CSD9jlUo19X/sPTfwOVW5oLLa/oLaDZi+ri8a3CWISDPrOYd2YU/OdWhQL1xrbCkbIFOjE9Fz2GC88efFtDNipKG870hjmnWM46zUHRSCNlBJms54YbeOTqanctdMhOk16DAQc/hTRpi/eO9JPJ2wCPfc/ZTLwLUVzmPrN9xwBOr7qdsYy17PsTsjMRFZlWohWtf3xY2JqlQe/TI8Tmu2zV21CpGzfqatIT92qfyOdet1sXfS8DNgjZE52PlVKsfGaCR/SAFIk6fw2avt9L1CraTUrPdaHaagf3lBXDVpELXw+MvLPoS5/XrjhTmJaGXZlwoYgVl/pp+MFPf9LxD/vAMBIzUu+upH1A4x+pFnuoWILb+XjG1aAPIEbQ510dqeeVjYqUWhhJO6m/hpP7X0Rdog2rgc0a5s8rkjz1OoqjNEvwW8kubPd8k5E4qGHTvSUNVsJCWddQs7guuawpHVuiiPtPlLviIpLY+ECS8ahlHzXfV9okqkMfAs2qiMp9bXnpK2LNPbu/5a0yNQLjU8LFclMo6736FN+wEY1L6KcZoGW5N+cKBK7WtROd0I+9mir5Hdq64SRiJ7xwL0a9QHsfOSkdCtrhUVBx13vK6T5k7lFkOwiM8whXNFY1e5ipXHz115dOz6WhudvappXVRhK0srIOHJX+1H/e2PIp4GaDu0noT/7RiBsOsjUL90EJJD66PfyBGorONwYHdSCnKDI7RmzPEC4pXLQqDYCFAYgqvLMTr1MxzFE35doGt+A/m6cJYCECUwUQIVu/DFLoBR19UxBSyllB6h3jfPmcIWLXxR10yhTCm1VdeUEFP5V8IXB9NwMD31tpfHEuSdYY7OUCxxyiNnpTyOivlACV+uKkPhCe9gakpDCIUjSgOGmi6GoMUmfOE1pxLMKC0YrcViXrMLX9R1dU3FYxe+6GPzXBleF1coAtFN7+KqLR/RcOZdrgfk8rUa8EvrLP11tIH5shrZpDswbRye5ov5iNa1cHL3OvQfNQ1dpqzGE7f6+gzH5saHarAK8znzgcx+3op/xIxIJNwXjazt7+GxJTRI94rxgKvjYp4+2X4X7omuAcu/7/xQRbkl8MKjTyLy3dGIoTbFJ88Oxnt7+9CwIF+Qdfo/Y+GKbahKFWI1JcfbhXA6Thcacn1hItD4uXmm4ITTHh6hgcQXB+PdG2ajS3R57Hj/WRrSBF5pxniDDlG9GFj44XrE9IlDVuIyvEANm+g77LErKP5dRPRdNL6pjNe1QEI1+guKYz7GYSllKc9zGo1ygctufjVcRyFW3BRttDSVq/K8RcHM85xqlHfoIz7QT8PDr86jQbxwrniiXgSo0XE1NxxClLNyqLfzFmErLRA2qMGfWfaFtWbraSCpn03GyGnbtIFTb0OP/M6KVl2vYx3u5QuAITSJbnkXskZ9xBULxqJqfuQ+6sXIT6H/lyf97xdi7a56NNT3M5a+PI5B3V9wK/DovUd7sU0oQ3409PjsszzDZWOjVGZy9Nfh9xato8HFloh0LafASy5nkXGd8LkTuH789BWfMXmeVNoLrdgmH3vifcwZ2x4Vfk7E04pnBwoA2M/StHc/efTuc9pb/v7kr/+pqFVLCex8p21oXVCzioZpx7zSFyEH1uKFWdsYlSVMy8EXc2bip1rdaOzTaOOudGjAufvN7MsPP4mI2WMRQ23quZw+YrkK11blLqcDHcpA+HU0hDnZMHDbyvIQYBvZ+iGETxuFN+jn+QXRfn2qdrPy6QcQ8495aBxBg8XjBlO41gLPc2pUhZP+0w+JMutrKKfuv9QZ4dmsr4dp4LUrjRtTCKJceNkKiLx1IKb23orHmEb0gvXUGuCstSrA1kWLsCO6L6oyXMKL5BXVWIcx/v2Mx2hs+J1/9OV4odrBKraDl3Q7SF2gDPpG4PUFL9Ngbi5ys1XNhUNrAVAYvHDmOkTe18dYKcgWY6o2BOwrnHvc89X3Y+7sru8Nj86oiufvrYecnavwSML7+t4QzaVZL25MZJvii5JnvwxB/EOd8MaoiXqc/IDjsC/nf6wLQUxPjuPU1nj3xumcnngdBR8cL+ft1WNZuDIAvC4VaelcujsvFZO0wWazrXr3I1fCqu1zOd8HaIg3bCA+mNQZ13OFlhyOu6hi5C/nwGa8+3EWug/p5MOop2pls9BjclW8w3vHEd7/Rq6w7n8F8DfzoHLg16l+1MTdjxqU5/j4rPr40QnxNF5tjf0e4f2W1cOXcXA1mdGlpnHaEDVqUldN1vcce6/yNX6oPNcK1H5oUF2NeY88uxgfDKeh7NOJrI9nsVX1IXNamU6Y/1R/u4KFILzx3/I3FnO2Lq9b2KFe8PtRODIJNFCK+L9y5PDhcvkeUBQ6ddp0p62Kf2LNsAbotJMrwpRPwcQphhBkUIu6OoXQYD5w060Z3QTd9r2I4QnPoCHDqTxO7FgVxybMRHxoGvoNe1n7+zT9PCrXuRsJQaFIWPMQarTfjTF/BT6a8IaeEnJvjCF88Bmv72LpeAvzr0jsmEcansXoNY+ievv/4OlG1yIhgXzpHht2t6nBETjVimE10XDofEyYUx2jk55Ev9dvx0cjG+OBUTGYO+Fl3NjgKN56shntoQzBxCVKg2c9Ml6/yEIGzpJcFQK/DwLBHKaDeVO82n3jDiRgCXStSAU6SwGITdBSSgtacj3OaSGKKXDRWjFewhctaNGaMQ638MUS0ijhixbIUHCjtV5Ueuf5XMAS8A22lH614ouGzV0y4YuKOKS0KXwpY2i8KMFKKIUjSviiBCbqWGu3qPNKw4XnlM6pErDoa2V5zvRjCWT0VvmjHyWYUfFY59S2jK83O1uBf4e74eqlGx+hy122x5XyUbiHLx47K7dyvayG1GiBJa9mcWWKUeg6zShIqyFTMNSPACRgUak9oh6M9bKTpkcr/kdGPYuu84yT7Z+aghHWl9EaVF+Pmoa3hvXGwoFcdrd/4Py0enoxTmIwX556G5HRvsbUJWqVFTr1QNg1Ci/wBajrh1x54yNj5Q3Do/U/Au2H1MPSaRmcb863EtNFthmNqT/zXvj0ALynz1HjhRb2jWxSQPTqQDIah65LeJFpduGDZ7IVWE1jCXP3e+u0xzY8Gl2otfHuna1ModR1uGtgFJbOikaMKZCyWPmuC/NRuEknYNkwdJ1oxN73lXl8uVLtsx1e6b2DL0298a6+dB0efuVNbd0/74S6zi9s5hdx9TJfi4Kvkb0f4OoCXMmGZX+dy1+OHDXAVPW+DkO56kR+AYiRZm3apMCSZVyiMUKfCI9V9i0+Qq1mbsEENBOz3+Srl06owC5ofeDWkWjNI9/9LLLdcDz86QOs8we011a9uTrF96kuoc5Jng1vUgtv8brxQN6IK2pw7r8uL+ea9ySzBK5i8tVKvWpJeXvevOtOqWnz5czDsd6UC1w/hh/Xf7M82maEOhmgfIwYYxa/Cjw6Cv27ztJR1OpAWwYjGxnReeeRZ3WNqrx69bkQP/3Jd//L0/XgPX3AqBuzLgKlTYHYExsXo8bkcRj/9CjUajkEr78SiZFPf2HUDVdrmTuL7eLVvkY5PP5zet4b85A2mF/UB9jqdd5HevwoH9sOQ5usdNV5dEv1dXC9a2yx5zFfW2Jf607Z5VvZAynkM8vhkbY6MM437toJS6mSP15fV1McnjC0OwKlz+ltY5ZMAf4+DI888L4OGd5yOJYMieN+njHFwUy2waA38UQSV3/qMRkxm59C/NhXsZQreTz2AIVcFFJ26doIO5N0FK5/0bGpjLejcdxkiF79SR3U6vAm+n7JlbZ6mNeoKfPKgm6GVkPmNry15H283megKx5rJ1C48oH6fhjH4ikn8ciwcehpjt3ue0P5oo+J9n7HXHv3S7WaUHhcKz2WZNFWU6Sfqgu5wf9Yh5Ycx3/kOJ5A4bgGcB2eMMeyvG60+bR5FNkq9kCrlhSArKug9737kXGSGdDjegjumTQWO3qTQwejf6IJBSJ9VX3TPNy297B0STT6DjdCef5XoxOneaS+ha4deIOha0wtIev+p+47/sZea9x2jSE6tNVyzQO241YvLUYWPwZY/QiV2nFlKdNOltf4oEPlO2fct+0xK/T6jhMWjam0UfMYPxCsVIFvbmFMWVNjD53qe97jh4pH5TmkWuD2o8a8kw+M4rhsPHi4+5CO2v2P/a2UWhnGfcbY41ou1Pw+xw+DDty6nrVuc/t78kmnEG7Dhk1ocdftKFOmDEqX5sNlIdz58+dx5swZrP/iazRvHl+IEAV4OXsQCeX+gldLNcfGoyvR8FrTf/pqNIq8B/+NnUqNgyG2+5IDi7v9H/p/0lEv31qHz6u+3Mcjq6L79Hb5/Dh2r8bwnl0wN4kvCnRKo2Tht7Nwbz0rYQc2vf4o/mYuIzudNjn60SZH9q5leOSvD2jDqipcUGxnLi/7ll4CVx0rw65vDuyF0aatECW8mfCv+XjcZZPEd7w67IX+Kyo75nHGyF4YPus7I0WvPKYtfxJ1u04im1+pKeMGu3vRk6jfy3b+6Ofo9H+ttIHUuTvpt44Da14aiE4Jy1wluTdhPt4c3UNrgviL1+VZdoSAEChZBM7wFmkJU9S0IiV80dOKzK3WbFH7atqQsXVNJdLX1JQjnrd+luaLpQWjtF30viV8ocRbCV8uh1PCl7Km8EUJXJSwxBK+KFstStCij/lEQAGK1nrJJ3wxhSuWxoslmNH+1DUVr5fwJZiCnt/KUcVYzz3mA5L1slysSav41VOVit/Pw61HegXkx5jPTNyFisweM78qJ7TAC8eewkY+H+TLCjOpOXAqSL5rZp6KnqY9/ULsF1B2FYMuv6+6upg8qrIz7ktevkIg8OXFZ5nNaRLt+aW9C194/U7j8RXhhZ4rRP1caNTWPP0iN+uCElR5Lkr/Kyg+dZ1CjpUfbEZEm06mgVtOI1lDQeGLwAebqRWUvorGQWfyK/xyvwI1HY3KmK+2rJLQmS7kmKEiU+4cl5K9szfSuOTyOz6WXDY8ef5X6fhq9wWlX9B1z1TcR77SU0tOq+kZH2yejkj2RFV0n1PSeMG7n6at4MvkxFrUiBtoare503Lt+QhXqGuB2vvFjDeuxN07eVxmWxnbHsP+3N6vAMv0HyjtAGVVY4S6CV3IfS5/ffN+Mpj3k1hqMHA5cm+XtmQAeq7sjI1zea/RefLTlgPk1ztOf8c6b9Ty8Nlm/AUq7PmLyV+g9sP08zPNn6nfRAhSikYF1S+QU4IX9StWIUigBC/hNSU8Ui5UPZQWwanVW5T2id9wjFdpquvr9Pe7dCqPzJjWTinOPJplV19glZkCcUJACAiB4iOgtE8oHPGecuQSvnBUs/bPqGlD1jHnA7gEMaa9F0vTRQtfTIGM2ncJX6jlovbVYK60Xi6HU98lruInlbKcduQlfNHaLHatFwpQPIQvpnDFpdmijylosbRlTKGLh/BFXVP+1JSyK9DlHVqPrlS5V9oCY+bxIfuGfGKOK7DUV3iRTu9ChxaD0UVptJgrFF3hJf4dFS8HC/u1xVt7OTWsZTtUPbYKa7/n/vDZeIcaWVlfjEOHp6sGfjEv5tKkraOB3wROIVH2aTa+6leboJiTLZbo8na/j2YDllFbiXYSlKZUEdxW2qYYGZZ/KkERorjMXilMmHg/XlhBbQna7dhIux0lY3TOwPgmD6DsPzh9lHamvF3qnI7oP6sz+4CprejtQY4LReCSCkGa39kYwVTlLoomyFnOf9+weWvxaIIUCoF4EgJCQAgIASFwGQhw9Su38EUJYZR2i6mpYhPIaE0XS/jiErooQQx/FLCU0gIaJWgxhTIUxGhtGH2sBC6MV9t+4ZQjJXxRxnYvh1MykKsoxfYQvlA4QqGLS+tFC1X4pM6tIXwx9rUQRV1TGjHajylYUUIXJbRRW33N0nxRftU+f7QtdUkd581vTaKNgRrRNHCa/4H1kqYtkV8iAlzyOZ3LNnKJIG+18UuUoERrJ3COBoS3bcMOLp2plsis1agxGtxwnfaRl8GlLzlBzrcdFnskxbevDJQmp+ciMlbZBCq+eH+TmGhrIiMzj8uZG/yKkmbWoUPI46ojvuwgFSWey+k3jUZXM85VQEwcjVxf4ltBsZWT7T+Nbf/6ajW4jG3+WPOyaUD3LFersq9ClN+bnCmAwCUVghSQdsDLxTIdJmAKclEICAEhIASEwB+QgBK+aGEK1Xe1IIVCEiVIUfvWNCJLCMNj1/Qi06/hj4IWpRGj/CthiyWQsY71NU45UsIXZWD3NPVp1TLTl8MFUxNVab6EUPNFG9lVAhQKR+zCF0uTRZ9TwhMlWKE/Pc3IELhooYw6p4Uxyo+1bwlmeE5dV3MO1DVZZvpy1LakKQSEgBAQAkKgQAKXbGKBCDEKZC8ehIAQEAJCQAj89gTUy3kol/dQP9MFEk8EumaFL9T2PAUhStNFCUxMIYvWfLGOLWGK1oYxhS/qmiWYMacauYQvtqlH2givFsAo/8rei9KoUcIXar4o4QuXmFbrqpfyMmsfeKJuoUrl31MZxq7mb4aGGNOOlEDFLnxRghalxaIEJkr44tJqUcfKr6HhooUvZcxzWrtFXeOPAhdjCWp1rAQwpmCGpRQnBISAEBACQkAI+CdwyYQg/pOUK0JACAgBISAEhMAfjoCalnIVBS/qZ7pAApZA16zwhdrqZaa9hC82TRdDK8YQnqh9YyUkHmuhjBKmGNOMPIQveqqREtSoaUj0o49N4ctpCl/UMtNq2hGNvePkZVhmmsIXbe+FAhgtTLFWOlJCF63hYgpfKGjRghRLGGNdo0DFQ/iihC5qKWmtGaNWQlJCGFPooq8pnW0RvhSqPf6ePGUd49IZlX9POZK8CAEhIAR+EwIiBPlNMEsiQkAICAEhIASEwGUhoJeZ5uPO1eVcyQcSsAS65oqgMDtqmWlL2OLSfKFwxeOcmkqkhCimQEXvm+c8hC88Z041MqYn8dgSvuiwnNqkhS/WMtMUvPzC9L3cJRVTqJWOtPCFAhFtcJeCFjX9iEIS1/LRlrBFnbP2leBFCVLMqUeeNl9MYYslfLH8WgKYErjMtFeVXNbD4IfvhbPB7Tj38FPA9ZXz5aXMzdXznZMTQuBiCJz5/vDFBJewQqDYCIgQpNhQSkRCQAgIASEgBISAEDAJqKWI1e9qtyXFQAKWQNeKxFQtM20JWpRmyxmlzeLWdDGumUIV+tPaLGoKkk3zxZhepMKZ/nhNa8goP9rIrtKUUYIbClqU8CVP2ZlRP65+lM30keOR5UsmfFERa+GLWulICV9MgYpP4YvSflEaLtxawhQlfHEJaUzNFuuaKWjx0HqxhC+/5TLTHiSL+eDUKZRetASlly/DuQcfwfneQ4FyFYo5EYlOCAgBIfD7IyBCkN9fnUiOhIAQEAJCQAgIASFwYQSUdoT6mS+zBQlXCrpeuEwwFiV8sYQttOWije3ajt3X1OpFlmBGCWdMQYsS2CiDusoOjJ5eZFxz2XvRRnZ57rQS3JiaL2qZaQeFL9SKKQXGiZMe2b1kwhe1zHRZNb2LwpeyXsIXClG0oMXSdLFPObIELFoQo6YVGYIZrQljXVOCGQpb8glf1HWl1VSMTtvOUfGRYdDb0xG0aA7O/X0UznfuawjwzLTk630xQv+DRiVaRX/Qiv8dF7t4R9PfcUEla0JACAgBISAEhIAQEAKXggDFDcpmiPqZii+BhCuBrhUtd4zJ0noxt27hixKoWMIWbvPswhcleFHXDAGMawUkJXxRPyXEsQQxSjCjBDQUvsChBD00squEL2rFI/5Kgdfxi0e2L5nwRS3xGcp/ytiuXfhCDRjX9CJL04Vbt70Xpf3CurELX9Txr0prx+Z+diDopXEoPfcfODc8wTDzUnyVZUtIdoWAEBACl5eACEEuL39JXQgIASEgBISAEBACQuCCCFDcoDQk1M90gd7ZA12zwhdqq5aZ1gIWJVxRwhQKSZTWi7WakbfwRWu4WEIXa2uGsYQtlkDGOlZh+DNWOqKwwmEa2z3F7SkKdPRKR9mu7Ban4KVU+nEEj3jcFbfsCAEhIASuNAIiBPlNavQcck+dQdly7pv0b5KsJCIEhIAQEAJCQAgIASFQvATUMtN6Kgs1LEwXSMAS6JoVvlBbtcy0S/iiNFTU1CBuXXZf1DnzR39am0X7t2u+mBoxp39F6RWf+k/WkqoUW+b9JyVXhIAQEAK/NYErVAjiwN4tiUjlmK/EDo4z5xB6zTW4pX4swtzG4X8z1odXr0DDzWfw4WNd0LSq0mUUJwSEgBAQAkJACAgBISAEikBALTMdyiWm1c90gWQUga7h1xzfQpCQUjjfpSvO9RqCMu3irWRkKwSEgBC4oghcoUKQPGxfk44RnLYZbZZw59kMYOUeTOraCD1uqX5pK3HPBvx5XhAOvBQPzrhE5Rsr4/G006h+/cUIQH7BshfXwNG5DXrUu+bS5l9iFwJCQAgIASEgBISAELhyCSgNEbsLC8G5ngNw/v6BwHV/sl+RfSEgBITAFUfgChWCGPX0Su826G8JDE4dxIK3t2PEkm2oXuPPaPonUyBx/jgyD2Yi+2wQqkfdpFdac9fyOWQfPohMGr4KqxSBShU9hQ+5Px7E4Z/zULZCRVSvWskIdt6BzB/PwHn2DI4dP47K5a5B2doNMbwap8PQgDiymdb5UITZ41LncA3Cwqi3YstPpWo3uTVX6OeYmkZ6PBO5nAJaNszMi81//vy7SyJ7QkAICAEhIASEgBAQAkJAE1DTZuic1SrifN+hON+hJ1e7cWuY6IvyTwgIASFwhRK4ooUgAOdOWq5cTfR4PA/fjk3CnC/3oWnn2sCR7Rgw9SBWW36QzCkrf+OUFQoj8vZhyrhETKQ2ieGS8HjzOIxueRMPHfj2g09xT/IZ6yKCwyth55PxyFyyCncmGuk2nLgOd8fF4J/1jiByXhbWPnc/Qjd8iTu/CUXShHYwxCaZmD5+E16PqYv/NjqJ9jPTsdMVayJmPdQG7f7vB/Qcn4zP1fmVzNPKIGwe1wVRPwXIvysO2RECQkAICAEhIASEgBAQAjYCXEb57KQ34by7AxB0MZrKtjhlVwgIASFQQgiolc7/OC6kBtqFA59nqaXMMjHn7YNYRw2O3a/cj4xX7sC0qmdw/9tfUSuDGhd70vEqymHtU12QMeF+bL4zFG9u+C8yaRA8N/lLLQAZ2zpOX9vdPwJnszIxZeNhRN3fBbt7VqTV7oo63g/ur8u11tXNJUjbJ4m6Q03FOYUt+83l1I7swzieebfZX3B4xzHAys+ElphVFRi0jCKRkLr4YEI8JlFkNbbrHUyTApCQwPlnlOKEgBAQAkJACJRMAsZH6pKZd8l1ySRw6gpqdHmeS/b6rBBOeXG27HTpBCCFyYPPjP2RTjqQ67VK8R+p9FJWIXA5CfyxhCAUQ1SiYdIzR09xtZYsrKeWR9fIisg9uAd7DzoQFVmG01hO4PApTjeJiceRcXeg+smD2LtnDwUjSpDBKS40tno45ReUDq6EB5sprRAgrPYdSBvRCMNu+7M+Dg1RWEsj1BfdP9XGNBpnnb7tkPa7d9sxlAoO1wZTo+7vhHUDYo387DlKtUTqsmSfhjFrM1QLUUKvUqZe6QrIv+FJ/gsBISAEhIAQuHgC2bs+x+L1Kfkj4j1z06JlSPop/6ULPrNvAa4qXxoJ63kfvIRu9/plWLz8c06H9Z2IKvPHy1djzUrrtwwfb/LBwAzu2PcdPl60Gmk5vuODYrVyq/7Q4suHZ3pMa+Xn2P3TBbyYZ+/BqhXbkMlnGZfjlOANK77E3uM2DdnzGdjCc8k/Mg21v/pLbFj3tf6t4v6W5IOu4CVhJzvpc2za7YOXWc69qpxeLnvXdixblwzs34SIF1dgAj9m/WYubw+eGP0vLNtTCIFFUTJFTeYnxq5BJKeAK83lDVP/hQEr9hQlBu1X8fTpCsPTIw8+Y5GTrJtVr61A5NhV/CxbnO4XxnthdV6cuZC4hMDvncAVPh3GG/9xfJt8DnXjKqNsGRos5eWFX+/hz/BXijSig8tpYUN28ibU+cB7WCqjr4H+SimLpzZX9k/VtRFU2yk/u9egebNyGLLyMB+EqmHLN2fQrfkNOuzeZSs4VcbzJh0c5lZR9LhSQP79JC6nhYAQEAJCQAgUmYAjYyP6tV2NOj/vQGx5d3DHnqX4W6+H8FH6ecS6T1/cXpX6mDzsGdSpc+3FxRMgtGPHNNRv8yg/QrTF1hN3I9bH01DG1jfQbdBqxLapD9C2OjhlFrfORev4usazgC3+pEXPoVGvl/WZyTt+xaBY84OFzQ/OHsTfOjbBthyy8nHZSq9Nm7YMlYE1a77Toafv+Bn9YovC4hQGfn0YY6vegMG3GBNvs7/7L3p9fQrdgw/ijbY3Gbnauwf3fZ2J+bfwgeL0T5i+OQMbyaFZ2SAKUM5xam4GQqpnIPXvtxfy+cZeWO57GYn3ulq0w7wU9Bz7XySoqcDKvpoPl/HlQ+jjWILDdVhfdlf6DLZ8nYENJ/diXc8Y25XjWLXwIMZUqo7Ot0XglaqnEHVjUTjborqg3fOg/i8cZ22CKX/xFKL8rqAh4WhXtxxuufF6nspDNtWbDzuoxlxEp3iqGPK5wvBsWc2Wh3wx/EFPeC9wEIpaf62IxzOvRVgxE1HmXi6kzos5GxKdEPhdE/Bx2/9d57domQumKoXNZX/7LZ7m8djqvMmdOaFvPq/0pPHUGLfB01yqQ5YtR8n5xkxt52Pfk8YKL+pmHjHnhBEbvxqdP+W2B6JOZn6biGOVaiKmekXDD/zfcMJuuwF3r0zGqnWJzE8Q1t5WkxHScCsFIN1vj8EbHerqOPbQGahRAAAeBUlEQVT+aynivzej40Y9M1ExxHBMXt08feff9CMbISAEhIAQEALFQKByfA/c65yAj7YdRWyLKq4Yty1MwFVt5iPeOpVzAmnpGXAEV0Sdm6yT9O44QcG/etg/gd1pGYiIrAtlCxw5R7E7/ThCy0cgspr5Alq+LvolPMNzNkkBw6twYLyRjNd1xYo3mOnu43V7PK5ceu2cTcHwxsNxb8KLSHvZEDR4+dCHR9OSEDtjF7YNNO7Jvvyoc7vnt0ejfqsxfcl8fHxfH4T6e7IKDkWbMkrA4d+p9D6y0jt7FDM61sWo6Z+j34zORHgCuaFk6Cq8wsrnkvLXeqYZVpMap/swfU+GKQQ5h+QUQy3kw10ZGE8hiPqOc3jPcU7dvQa3UEMWeSrTQfhirFvIkP3tBtRZko5V1FToXLscsn/8BWF/uoYG4/ch88w1FBhEMIzNgPx1NCDP69r5MhIfYnzUcRuVr0Sj8tYzkxEMpzKwN52aEXx+q36j9XHpHHKP5vBr+TkcO5qB6mEVaRzeBsEMitC6qOpuGdZZbqujy2078OY3hxlHjGmPjad/3KNXEZzW/C9U6b0GPfpGuI3Oq9A+Dc87TA6mMXxV/h+zEEa7cFQApjOP/2RdV+dsLi8Thw8ep6Yvy16FGsO2S3o3OwOHj/6CXJY/iuU3nP/yu1naDPRzOnbz7neQoaoLQ8vEIzd55oIAwSGoXrMmyvrSWlYJkydgewhV57QrBE+2K3cerHC2tlLlJlQyP/LlHme/LUf2pnDLOK7EY7O96OvuYys2vVV1tFctLhCESjW5mICHgMyWnr1tMqBOI4xpZh/G3szTCLNfJ5/sU0FcwIDP9vszqClGTrXJyZ6wz7ZhebClW6kGF1Uwa9nHAgdRze/A8LNlbHHbwtrzVFCeraRtW486Z9qZeWXYP83+5qudqXJnn2Eft4XULNg9Knr1U1s6sisESioBf7fqkloeV75DOVrt3ZOMvWc5+Jw/j9TEAxiYcgbBYZzG0kjduCuiR3gyBn6wAVG9/4qYilRL+yAZI7OuQcqENgi7Cjh75BRSD2eg8ul0TJiTxTD8WkIX1agSnIkZePC9rzGe0u7sXf9Fqw2/4KmO1SgEoQcKSZw4jqXb9qBdzE2umxwFs4YLqYXBNybjvg1ZCKnKMFoEzKk6vLol/SeuLsNBd89/tYHVYJd4mF8KWKZXNiahRdW/IKoqDb0Gyr+VlmyFgBAQAkJACFwsgeC6GPRUFXSe8jlGt+hh3NcoTJgx8Sge+/RufXzs39MQ2eRRV0qhbf6B3cuHoDKfNHbP6oW4kbw3nl2tryttiTYHX0bdrob2hDrp8s9446+NRr+kUxhULxTHNjHeFu54S5UdgK2HZiKWn6lVvLfMCUV00kdI4p1XudiE9dj27N1639e/f/MeP6/CePwwsi0eeXmrLy8850DapydR/zl+fPjpIDKOOVDxz3VR2ZTT2ANVrNIDm9JXomGVFKyhWKHYXHAVRJQvg+DrVaIOfNy8JoY1W4qM182y5XyHitc3wARqijzuoSlSETH1ymDXN1xNjiHLnj+MDfuBttXLYPXhn/SU3yh+7Pl21xm+597g8RXaob7vmC+SYXE10HZJlqGpcOq/+PskTgXidN7PKU9Rds8OjnNgwaTteDrbKnESut9WG290jsVeH0biP7i/Zj6j8nfXrYkP+t6qI8jctg6xyykgcLkdtMvWAbUyNyFSP4MB9739pZ6OfOAl8wOVy2/gnag7qgHfHMQWLdAxBDV7t2YwUDk0VasInkpBexqg79G7JVcV5AufP8P5YQfRdZLyZ64+uOdL1JmTSXtt8YbA6fDXqPN2Bj556n7c4vXemLv/azxI4/efm1lV2seqxTY3j73LH1RuDxKfaomwg77KH4ftM9fhvv18NjRdSPVqptbOL1jA6TDP0eD+/p41rMt6m7v/Sxrgz/AwwP/hiI7uFRM9fPs/KJAnhS/uPFD7hm1wzsRtHm1lbPtbMbhpOJZO/BJjuYjAPmVDj1OE/j4xCRvr1saBvrEMtw9jJiaiesd4DOOzt4c7ksjFDfbZFjdIxDTazet8C5/x8w5ijp+2qQRDSyd9iTlhZbAzy/qgmYSx7RsxP9WRu/db1Jl3HG1ZP6vNqXKlgvfgP+PaoJISGPlrG2pRBZZzAcs5wrtPtA/xucABlq/AXYnhODihOcpeRJ49uHgd5O5i+5mXieiY2tSEqgh/7azSwSTdluc/1gXNlWCULnnhOrROYV+nnUK+gogTAlcUAX/y35JfSA5eM74+iDvnpeDOD/ZQAHIeY2+vjX1jrBtnKNo9fismhZ/BffO2o85UCkCyQ7HisXj9QHBLu5pUGz2FVrzhxs45iLCqhgBEg6l+B5I6VkJmSjoaTt2mBSCD4m5yDdBlI6vhKT4ojFiehK7LU1GWedE2QlxRBOHW243BPOH2m0zWEejSkVZbKXSJ5eB858pfMMglAFFeKuKWRuW0AdY7p3IO76nA+TcjlY0QEAJCQAgIgWIh0KjXRJxe0wtJVD5QLjtpNVYEtUa3ptT44Av5oKaP4/H5O3D67HmcProDj6wdgXtnfGd4vjYCTtoSmPvvIzhO1fxB9U5gxv2v4fEl+w3/6V+h9bp/YtM+fi7gPTOylPmVnPHe2+Ix3DtlPY6reE/swvhmS9Cs3wKKBegY7znarhielKXj+W75M/j+pTeQ5PrqYCRv/XfsXoD4hP9h9lcjEMZ0lJDAn7sqqgzmdq2HKlVuRP3Yeoi8vjRXZ8tvJ6MyhUINiUBlKNcUxPiLM+D50FAKjWinZNECLJ4/GxMHXY/uy2/DR8OU0INaJG+PRPa0t7HbfDE7tm0ZSl89At3q5ZfMRMVW1B9jUpUCSHo6ZvAjzuj7/oJBtG22PU1pB6RjC1/UnqzHF0abC+UHIJdLz3S/YJYJ1oKuL/khKWlMGxx55W6kfkht1uwy+PChltpg++bW12DhN3uwbNdxn0biLaPy03oqA+/304h8JXyewpfVZE49zk7CcApA7q5bHWnKWP1TMXiceW0/LxFlazdHxpjauFtpzlIo8kMRBSC6PKY9thGb95nFy8T6r89wBb8bDM0QPp+pb1jGi14Aw/Pl/ozBfL5bv+sHHc9ellW5ZdSwUS5Ta9dURJSXAEQZ418wJx1flKuIf4+hwX3anJtJ4/cuZ5Z/0O11NZuMEXXx0Knj6Plhsu/yZx/CMgq2LJZpNNCfd5h1etho+KHMYyPWmadzYMuqDOwOj0Aa+We80givhJ3Dsq0WE0/fAY8K4snA9jwkL1TCslCsHdGB5euAtbeHYhxXO0zOvga33lYGjj3HdF/MPZih25xDa3cwEmqVLeSmeV0vAYji+fY+rCl3jcHzlXjMZwUOWZLIK3x5D9A2eVnnbWdWiG5PGXzBV2FfXLPXGA+CjYf16rcZdZHWvxqFt7SzkaLqOkDbUOmynCOyg9x94s5yuk9sya7lY4EDBuDCCaX04gkXmWdG5e1CQ8tACb2UAKRtXF0KQGJd/cxXO0Pt2niKkSzYZrWHwywzOGXf0Bzzjl+OhUBJJ+A9Qpb08pj5vwadx9yPzgWVJoTaFE/yl2c+LYXYFBOr3oo3XorDG2raizJGSnHR87b4KjWKx7rbGE49jFDF1VCDND2Uq41hz/Hn8q8GUteB3ilbLz7fuUqNmrvjNPNiT7N6y3bIaGmPJ0D+7d5kXwgIASEgBITARRIIrXM3ngoqi7mbDqJhx5rYOvN1hA39AHV4C3TsSsQa53l0iwpF2m4+OXMqS/0Hw/D2nEQ4htZnyrQvETMCDzSoYuYiFJGlgzF61j8RH9ED9es1xqLcHcY13lctYYLjUCK+D/4b5g4ytE3AqTKPvPwSxjRcgD2OHnxpNeM1BQF12vRA63Pj8d1+B2KpReLpTuCd2IcQO+ErPFCHV9T9m874UGHsu/6fzcB3y04idvQSfJbQmR9HHNg0oxdtetyIhkfPI96XsYRieKLKW78Vm+qkUagEHE8/rTVnPl3/HRp2q4+wBp3R4WwCPqMUqk6Da7Fp1gzEjF2rNW1c+bZ2alZDd76wJXNqSWVqf5TmNKIovrg2D0/CC3xh71Exky+XFCpEuV8uQzmVY8G8dajOcjh+zcG4/Wf4glYR7ZRmRN5RLXSa0LoRpzEorr9gb8o5ChGi0fRG440/qllDTNqwDtP/k47ODGMYiecLp/m5TRmVV8KcqKtPYe+uPSh79TUUyvBl9vtj6FH6J2pIBGFz90bG9IyKdTF6zLV48Dzf5pW7ylRPsT+nGVcK+d+wx+ZYeQiHz8ehevoevTLfh01rucKbT4L5Dc9TNUYbzj+iDOdz+lCDMnj062PA/X/G9sQzGFQ3FDP4USybVnFS/+NAvZgoD+0anYBpzP7V3g05nUd9ZY9Au+6cCj3REKrlUgtYaYh0qRrCaUp7dHlj+F3snRRTm8e7/GGcOj3hL8j98QdtvB8nadOO4Y+dPM3/xld87ng5ahyHsdkzzqWcjt20Xk3057PyhbmCebrjPY69B85xink4wqhZTVM0CK2qtHEy8e3h4+hPgd25b46zXnjq2yxEVy2HSkeOU0ACxCihEhcPqMV8ezjyXMX++1rv202eldB8UEskHT1DodYv2BKwbdbQff/u226iFrhqy6Fo2jwczjm/aC0pykPogtCjDTVY6MrWjub0snQs3XUU/SMpAGO6rkUVPNqG0SeiqdXi6hNt22D3XzMRGq7qxFjgINda4IBn3K6g/hQ4z1FmN3HFxz68+utERH4NamfVpXaWUZbA7SwG7SiQepXTxrI710bYngN4kxHqKfuuiGVHCFw5BIrhln0FwPB7U+WgVc7fzYTlLs3B07wvFxuFC4nTb/6LLVcSkRAQAkJACPzhCVRBt2nN0fDFrZjcxoF3Zv+I8f+53aASZKgQ9GtYz0UpNrY+DYEbL8jWSfWiqV47qMKBfvu+RO5Lz6FTk5f1mau6voav545AHfuTiYo3yEtbgV/TlbOrZ7vjVed9T0k5tnISRp87jbkta9K2xFHgBKe5nMtAGldji7ixioetDQTXxATrA4lOLRTxg15Em+HLsZtTY+KvN0qhLxXXP4cDN7/1GqZbNkEwE8fWP4ea7cmo40oaUzWmJPVYmMLpL6GYvORnjHm9vu/US9MwZXgiFiTtQeihM2hzWzXtL6ZxOexacxjflj9NwUj+l8vD2XlaKJRLOwiv3F4NnTllweP986x6yTbLzgqg8orNBUFNRUY+DQTTS7CShjjQamaiK0w067p5BT5I6a/hprTEukqbDW4RjXXywrdh9Q17bFv2HsetuzL1y/Wtptq/R6xUBKjME/4M51f/6/U4v/kn7D1ykC/iZZDQkTZFUqjVcHiPeodH/w5/9ohOHxjKBZ62W6jF0NbyaWoDDFmSZJ2hoX4aqa3k5yEzOxkvjOd0NJfvwu3EdL0Vs5ZwKviGfYD6UUNo/kPxaG4KsgoXi+Gr0DyVUIZlOZuViYZvKz0Nw0WzzGHK7kfNGloYlrx3HxwUXgzuH4dKy9TqRMzfXgeaxdX26Os6tMXTikxtS1fkCpBqh8K2wrRNapa5nXrWtx/zUM2U8cZfQNtQ6VbX7dyKmWWs6h6/XII267J9Wxx5tsdn7Z+1drgtoJ2paU5OThtL5ipSlb9VthEjzCn7tjhkVwhcIQTsjxpXSJGkGEJACAgBISAEhMClIFCn/d+RN6g93nx9ANaWG4MZtc23YAoXULo+tv3K1WNsTxYOPoB7vCfbMhXKVWAen7GSP2pp716NfjEdMLdXT9Bkh9sxXueZROSqB3kz3ux04+u525PnnqVF4nmWmhVHv9On+jWwz0MAOsVWRewUGkAdWtcdhHZJEho+ifqLV+Lem8zTZx3IcDpdGiRuz8W45/WWVDmiikfkjXol4ESDf+LjW67FzmunIr6ax2XbQShibimH1Z8d1NML5v/V8BjG6S/NVu7DPZvVF2LPl0sHX1YThrZDlJcswhap524uZ/CezLOdO6W/pHsKLmwvlnzpLAXT/oEVSgmaqE2bS00K9dbpUDIW88Uzd38ykn8tj1tiqDGhHe1fqHZwoa4c7bFVTUb3FV/jvqxzeMRcmS9fdHz5pZ6Hf8Pz5Wpwqk4GDeHuw2ZOD4oKu4k2FBLxwuL/0tZGGWyuadgc8YhXvVDTZf/qtuGBo1m6bpqrC+YKMWvH3Y8Y14u3A7k0Zsl3Y9O5y5/JF1U1xWnzmE5Mn5f16jHJlkf/2zLUQOlbk7lXxkH34J+TkvDgqj049Fgj/2H8XSksTxWe07KiaaPEY3WeUxRWUBCk7LI0r7odL6xIxk4Klf5Ng7thbLv3r2H9s767tKYWhLczeR47aeOZvQ/f0vZfTKM/63luBbdN70gLcRywbeSp9aOw8STbtMtRo2XLQVSOidVaOGosdC1w4PJj7hSqP3kH8nNMbsoGyOwbM9BweQqq/ymM9oSo41JQO9PTnDhFbd2XiGJhnu5a208CcloIlHwChb3VlfySSgmEgBAQAkJACAiBiyNQ5W7MbVMRCQmz0eqlzq6pGKG17+aL4S60HDobu49y9bXdn2NwSAhumrDVlZ4T6g3XdBQyNAoNQqe3Pkd2jgMOrumoBAwRf+Jrgu1F14r37n7TjHiTlqFfy+dx7cgn9DQcFZtHvFb8PrZ1+q3EcS5Pe5o2SfTvpx16tZZNnN6iBCDZO2Zz1kUDrFHv48ERqLhzI/oOm0QV+xNw/JSCxc+2xfdlOiGegh8Pvz7SutBTaWnfYTenE+3elYKkTcswus8olAqtjyqmACi0Tjs8f/YDdOs1Cb1ntfXU0vBKtFJ0uJ4ioV4yY6qboqiKNdHOVJ1vGuv+Qm0F1UII68C+NV863ac4LYR2HXYmp2D6xhSukEKtiJlfYyI9DLammLAeLSPx2VxyN6oBjcojC2P+tV0bgN+7havujV2BKTTgWrau0gYA2kxdQ42KDGQmKyOiKbhv23EjSTP9OZ8xbLaaVnMhjvbYOO3hbNYpTgUqw2VxLeEK47KXr5wyPM8XQBrO37LrILKPpGDBa/9CzRc3ccqLctXR4kZqihw5h4G3VNNnYuLKaSObIVUr+17Cl7ZEelBY8fQHm7CBcWbuT8SEt1VDM1zZKKP8bd/cQA0TruSxZxueGL0Cdd4zNUO8yl9Wac8w06mHVf72YMGbyS6Dq1ac+bc0VjpuBaq9uA57fzxOgVOeFi4GqZUALsgF4OkRn7utLNhitJUt//oIES+uwbe6eoNg58dXdYRFV0K9s2dYJi71W9Nsu/Y4WUcPUpY5bskmrKLGSObhZEyfmogOyw9R18idnt+2ybjcOinuiO3iC/u+8qH9B2wb16Bpo1CcTtmD6euS2cYPYsN7n6MVhY6HzytNEwpsKNFSCxyoOvZ0xZNne5yVynBVm0Zt8EndIEz8bBuNI3P1y4LaGdk1b0bhaWImp8LQ9uDNniJNe/yyLwRKOgHztlrSiyH5FwJCQAgIASEgBC49AU4LGfYEJ4ovw+NdbVMxOH0k4cBqZN/QBvVnPaSzETtsPnaMbqz3QylUCI6wvWxxtZm5y6eifscWqGIa0OpH46ePNKSRT9rjqELDqFep9wYzXtx7D+pXe1TH1Wb0fKQk3O07Xp5V02F0WO3D9o9PPB7L15pL8IaWN/w4Th6lRvx3Wiijpus8susT7P5LGzSq9qT2UKpMZyz7br4Wvhzz8Ot+SfObti0bfndDI5A9pRfqT3H7iB3IKUKrjBV2jLNVcO+0tkj4+0H0i7e9xLuDuPf+VAOdgw9zSbtqtmklFXErV47BN0F8ufShseAO7blXprSeIhJqqtOri9XbxuOTnE2457NkjPtMnQnSq3M0NaeYGEbis7SR+Dn7z1MLgEbl229ALI3LLkw0tHncRuUr4fnHKNygsUtlkF656KqV8O++cXofFCI8eON/0YvhFn3/Cw6+xNU0jCtF+l+Wq+E8RUHMKmobGCvzmcE9ymcann9zuzacr3yUoraKZThfHUfVrwjsP47mf1Wv7Hxpj6mGu1fuQQy3vt01aPdYLMa+kYReNMav3KC65RDNpYs105CbMPrvXKL0bRr0n7pJXw9Y/rG1MenL7Vzh0IirrVqZhK//Vv1oOzfmtAxjX0V5Dbr0roZvaaD1zknrdBrB4RXxeddYvX8h//zyZGT2PKi28uHxDbhvZTJGrDRSmtTxVtcKOha/W+NMfmy7PYIP4gW2geo+P9eG0gbIrZg2QzEwplfZ6yisoLbJscBz2orKE40xs2vofJv7Rk6N/4b/AtpGy1b45Pha3LMhBeM2GOGmdWxkltNc4GBDJus4C2uf60LjzKVRymzIBfanAHm251Pvq7c79aO7pe8dmDZ+E3rN3ICkV9oFbmf0H1a/OttyCjLrVkN1l1aSjkr+CYErikCp7Oxs6nZ6Oie/xpw7dw4Ozk+9df2PHhf397zZ41gOhIAQEAJCQAgIASHgIkANAO3Mh3DXeX87hfXP6Sh8zfMUZPiLsxjPO06coKHWUIRdq140L8JR+6VThSeRcFzZ97jweNaM/DN6pr+GrEU9LjyS4gzJKS25Z2hLRE9tKETE5/mNXdW5t1F5HZRfy/OU2gPfRpW9iCK43bPao5/jNc9pTUUI79OrZRemOG2vmVOAPAzquxIvYvl1/orOCoUIp3jGTP1e5+zM9xSmFYe7VDxV3nzVEdMrUtssShkDlSXQtYLSuJR5dqUdoJ1xmecIailNe6gDOt94EQOVKy1jp8zNhsCw2NqSV/xyKASKSqCwjyhFjVf8CwEhIASEgBAQAn9EAkV9siisf740F98jeeErJvTaa4s1XeNLc+HTd/nctxqN6tyDJE4qmbuzs+v0Zd/hy2dZXy+g/jIW0AA8BR9FFH64k8nwb4DG7aloe0UpV2FjDhhnEcsfMK4AGSpUOPIsbleodIuYaKA4ea1IbbMoSReQblGi8vB7KfPsSshXO3Ngw9sr0IvyruDwSmhXjAIQV7KyIwR+RwREE+R3VBmSFSEgBISAEBACQkAI+CSQcxCbvj2IKjfdjjrVLoc4yGeu5OQVTEC+3l/BlZuvaOdw+NskHD57FW7lsrplfU5Dyheo0CekLRUalXj8jQgU9vvLb5QdSUYICAEhIASEgBAQAkIgH4HyNRFfkB2QfIHkhBAQAkKgMASCUP2WOJr9FScE/hgEilnO98eAJqUUAkJACAgBISAEhIAQEAJCQAgIASEgBEoeARGClLw6kxwLASEgBISAEBACQkAICAEhIASEgBAQAhdAQIQgFwBNgggBISAEhIAQEAJCQAgIASEgBISAEBACJY+ACEFKXp1JjoWAEBACQkAICAEhIASEgBAQAkJACAiBCyAgQpALgCZBhIAQEAJCQAgIASEgBISAEBACQkAICIGSR0CEICWvziTHQkAICAEhIASEgBAQAkJACAgBISAEhMAFEBAhyAVAkyBCQAgIASEgBISAEBACQkAICAEhIASEQMkjIEKQkldnkmMhIASEgBAQAkJACAgBISAEhIAQEAJC4AIIBF9AGAkiBISAEBACQkAICAEhIASEwB+AQJmbq/8BSilFFAJC4I9EQDRB/ki1LWUVAkJACAgBISAEhIAQEAJCQAgIASHwByYgmiB/4MqXogsBISAEhIAQEAJCQAgIAV8Eznx/2NdpOScEhIAQKPEERBOkxFehFEAICAEhIASEgBAQAkJACAgBISAEhIAQKAwBEYIUhpL4EQJCQAgIASEgBISAEBACQkAICAEhIARKPAERgpT4KpQCCAEhIASEgBAQAkJACAgBISAEhIAQEAKFISBCkMJQEj9CQAgIASEgBISAEBACQkAICAEhIASEQIknUGTDqBlHjpT4QksBhIAQEAJCQAgIASEgBISAEBACQkAICIE/HoEiC0Eiqlb941GSEgsBISAEhIAQEAJCQAgIASEgBISAEBACJZ6ATIcp8VUoBRACQkAICAEhIASEgBAQAkJACAgBISAECkNAhCCFoSR+hIAQEAJCQAgIASEgBISAEBACQkAICIEST0CEICW+CqUAQkAICAEhIASEgBAQAkJACAgBISAEhEBhCIgQpDCUxI8QEAJCQAgIASEgBISAEBACQkAICAEhUOIJiBCkxFehFEAICAEhIASEgBAQAkJACAgBISAEhIAQKAwBEYIUhpL4EQJCQAgIASEgBISAEBACQkAICAEhIARKPAERgpT4KpQCCAEhIASEgBAQAkJACAgBISAEhIAQEAKFIfD/kPn8u5k7SrYAAAAASUVORK5CYII=') no-repeat;
        }

        .mwp-notice-left {
            width: 132px;
            border-radius: 4px 0 0 4px;
            background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAADwCAYAAACKeki0AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsSAAALEgHS3X78AAA+w0lEQVR42u2deXwcdfnH32nSu5SWdltKw1Io5Sg3KIeAoKCIRmZBUERFBQGLIiqH+FtvXATFA0UREQ9E8ODIQkCUuwgI5SyFUqBQ0pS2TO8rTZrj98fnO8zsZLPZTZMmmz7v16uvZndn59qZzzzX9/mCYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGkUtFX++AMYDxMqOAw4DzgB2AnwEPkE0v6etdMwY2JmxGz+JlKoARwLuArwBHAgn36XrgOeDXwL+BVWTTbX29y8bAw4TN6Dm8zHjgw8BngPcBlQWWfgn4A3AH8CrZdF/vvTGAMGEzNg8vUwWMB04CPgXsC4xyn7YjK+0pYBnwfmAMMMh93gy8hsTtj0A9sNFEzthcTNiM7iGXcxdknX0M2Du2xEbgP8DNwO1k0014mcOBU4GPAxNiyy9BAvcn4Gmy6ea+PkSjfDFhM4rHywBsB0wFTkduZxKocku0AUuBh4HfAc8Cq8mm2yPrGAbsSihwOwFD3KftwNvAEygONwdYYnE4o1RM2Izi8DKTgBOAjwJHANvGllgC3AL8FVlcm7pYXwWwM3JhP4MsvkGRJZqA54E7kSX3Qo5AGkYBTNiM/Mg62x7YCzgN+CByH4dEltqErKo7gd8DS0t2Ib1MJTAaCeangUPc64AWYDXwJHCD+/9NsunWvj5FRv/FhM3oiJfZETgRWVMHkCs0IJfxUeAfyJqqL2RN+TXVOwFjEnUNz3ex3W2RgKbctofFlmgG5gL3AP8EnjErzsiHCZsBXmYEsBtwKFCDspdDyXUNm4GXgfuBG5GltqkzYfFrqkcABwOfQNnSYSiZ8GfgAWB5oq4h376AykR2AE5GArcPua5vO7LkZgN1wH3APLJpv69PpdE/MGHbGlF8qwoVztagJMA+KJAfrT1rQ7GuR4GbgEeANzpzA/2a6gpgMBptcDYSyO1ji60DHkPieDuwIVHXkD85oP2cAByEEg0fRQIX3ccg4fAy8CCQRVbdJqDNSke2TkzYtiZkme0DHIjcvSPQKIE4G4AXgbuBW8mmXyi0Wr+mGiRAxwBfRMI2uIu9aQdeQbG5O4DXOhW4cP8nIgvOQ2I3Ps9SrW7fs8B/UQLibXNZty5M2AYqXmYQEq3RwHTgQ2iY0y7IiooKTxsqpF0E3ItE4TVgYWelFk7MhiBB+wBwjtvONiXuaTPwOoqZ/Q1YADQm6ho6FyKNcJiC4nEfdtsdleeYVrr1vQzcBcwCfGBDl1lbo6wxYRtIyHXbHgX8D0AZxkPoWAwbsBF4BrmY9wOPkk1v6GozLn72XiQsHwcm99ARrEcCdA+yFpfmjcPlHvMgYD/gaDSMq9DxNgJPozq5WWjc6nyy6ZYe2n+jn2DCVq7oht4GWWQ7ImvsGGB3dGNvS27wH2QdrUA1Z3VoIPprwLKubm6/pno4ErDjgVNQGciYPNvoCTYgK+7fKLb3OrC6Cyuuwh3zZORinwjsAYwDRpJ7rQdDvZYhi+5h4H/Aq6i0ZJWJXXljwlaOeJmjkHWyH7AnquTvbMB5CxKvx5CF8j9gLtn0umI25ayzY1GS4UNANVv2ulmDhOdOoC5R17C4yHNUhZIhB7vzdCSKLxZylRcjt/U59+9vNrSrPDFhK0e8zG0ogB63ltqRECxGYvYwyhQudO83dRVE92uqBwETkSh8BAnaVBTD6svrZSMarjUTuBV1B6lP1DU0dXGuQBngbYCxyJo7Grnqk1ACIv5QaEdW7QFk00v78JiNbmLCVo54mX+j+FbAChQrm4lqu15ARbNFu1N+TXUVymZ+EDgKdenYttjvb2E2AfNR1vNB4F+JuoaVJZy/CiRoeyEr7nCUZZ1KeE+sBPYhm17U1wdrlI4JWzniZR5EVkc78D3gKlRv1kyRtVt+TXVQBDsdZTU/imJ18cLc/kw7ErlVSNRvR+Udr6PMajHnMqi9m4xietPcJyuB/cmm6/v6II3Sqdr8VRh9QLSbxutk06sLLexKMyrcv+2A9wDHISulOyUa/YUKwpKTk5F7/jqqY5vp11Q/gIp1W4H2vEKXTbe7eNyXkMUWXXehRplGP8aErTwJBqK3oeRAXvya6qGog8Y0JGZHonKIgfq7D0ZZ4d1RIS8o6/kIcK9fUz0PJQfWvCNyisGdBXyZjpbqQD1PAx774cqToBC1DVkj7+BczAPRcKYjUMZ0MuVrlW0uU9y/T6CEwOvAo35N9TWJyhlvIaG/ALngy5GVNgZZbOXikhsxTNjKn3icNIEC6iP7esf6GUNQU8zxqEB3mXv9cxRbBPgtsvTGoPidDcMqU+yJVJ4Ew5wq6fhwCgLqRkfWApk2Ki5LVM4YhhIvB7vPnkZjVqNDyKxIt0wxYStPAvczn7CBCVs+VgKXApdPrPxiO3AFaqA5CHUc+RUadRBYaUFrJKMMMWErTwJhU/shBcADzGLryErgK+3wy0TljG2QwH2eMAnzBBpiFn9QmLCVKSZs5UnUXRoS+8wsjVwWAl9N1DXcOKFyRjvwHdQrLppZvppsejkStSAx004sMWOUD5Y8KE+iN9xQZLlFXSiz2HQOHkdlHC/hZUYDlwDnkZtVrkMdfUFiF219ZA+IMsUstvKkMfJ3Pottaxe2NuB6NDnMC4nKGW3AN8kVNVD5x49RUgE6WmwmbGWKCVt5sjzy99DYZ1u7sL2O4mfnJ+oaFiYqZ+yEJmH+BuEM9aBB9d9FPegCa3cwJmwDAnNFy5PoEKq4sLUSWiBbE22oZOMbwMOJyhnteEwBrkPjaitiy94B3BIbVxsVtmC+B6MMMYutPIlabNuTe9O2IBdra8JHhbbHJ+oaHnSu54dQF95j6fgAnwtcQja9Kvb+YELXfrk1myxfzGIrT1ZF/p5ER2HbWqaha0bdPL4JPJaoa2jEywxDowcyaChVnDVAhmz6jTyfDSMUNuvDVsaYsJUnKyJ/TyZX2DaxddyUPnAlcH2irkEWrCZ5OR/4KrnxtIBW4Ddospp8TCLs6LG1Wb0DChO28iTaHnsHOsaPliHLbSD+vo1oguQrgCcSdQ0tru3QvsDlaJKZoXm+14Lai1/ut17TTE31/sCribqG9ZFldoz8vTU8HAYsFmMrT95GWT3QoO53bmTXjmc5AzPw/RJKDnwqUdfwaETUzgJuQQ0zh3by3QeBr7nedQeiUpA4JmwDBBO28mQ9YRxtEHKhoiwjFL5ypw3NHvVd1Lb8N4m6BmV9vcyewLXAT1HfuXy0A3OAM7dnfb1fU70j8BO33vg52inyt7miZcxAdFW2BhqRsAUWxg7o5g9YwcAQtg2o0PaPwHPvTL/nZSqRy/kz5IIWekC/DnyJbHrhCzXVg4EL0VSFtyXqGuJDpqLzo77d1wdvdB8TtvJkPbk33k6xzxeijhXlykoU4P8lmpimJVHXEAjabsDFqBX4qC7WswQ4A3jENeD8JGoBvgF4M2dJZVMnuler3D4YZYq5ouXJBuRuBkyLdfjwgbf6eidLJKj0vx34HHBuoq7h2URdQ0uicgZ4maHAmcA/gdPpWtSWAf9HOLJgf1QWUonKROKzT00ifNAvxYStrDGLrTxpRPOGtqGH036oBmsjQKKuoc2vqX4STarc3wnm8KxDowSeypnx3csMccfxLTRvQzEP44XADLLpuwD8muok8Bc0MzzuPL0Z+84+hKUeDeQ+OIwyw4StHMmmwcvMQ5bHMGRtTCT3Zn2ur3ezC1pR9vYWJDrPJ+oaNLhf1melO66LgFPomCDpjAXAV9BUevg11dUoFrdnZJk1REdvaHt7EQrbYnKLoI0yw4StfHkBlXQMA6pRAiEqbHOQZTe8r3c0Rjsq27gD+AMwP8dCE5NQbOwMYJcS1j0X+CLZ9EwAv6Z6GyRqJ8WWezaWOBiKpiEc5PZvLtn01txIoOwxYStf5qMEwbZoApck6j8WsAplBPfq6x1FLvNqZEXeDNwLLErUNYTi4WUGoTlPT0GCth+5LYYK0Y7mEv0C8BSAX1M9GrmvKTpOePNI7PV27vwF+zq3r0+YsXmYsJUr2fRGvMxcVKJQiYLjf48ssQaYR98KWzsK0t+GBqTfn6hr6Diw3MuMQFnOM9HcpxUlbuN/wJfJpp8B8GuqB6FhVfH+a8F5eSb23jhCYQvq3owyxoStvHkCda8AeHfss/VI2IIEw5aiHQXeXwFuAv4FvJWoa8gdCeFlglnpD0fjOw+h9CkDNwL3IAFbBODXVI8AZgBfR256nJfJHZIG6pAS1LCtAfINkDfKCBO28uZ/aND7YGAfvMzgIDaUqGto9Wuqn0alIV2VRvQEbUg0atHwpSeJzrgeRRbaR1BDyKOAEd3YXivqfvsrsullAH5NdRVqBf7tAsc8j47dT/YitOxmWbui8seErbyZj+Jou6NJfg8DZkY+fwgV8vaWsDUioXgSZTbnoCaXrZ0I2hRUunG+2+dhRW4n33H/ALg5EHK/pnoUkEbWW2eW3yZk5YajMpQR/aB71Y7if0aZY8JW3ixGN/nu6Lc8Bi/zSNDqOlHXsNzVs5WSWSyGpShmdh/wKNCQZ3hSiJcZC3wCBff3pvOB6sUwF7ma/yWbbgXwa6qHAReguFohsdwEzIqJbgIlKkAJjtk9fK6MPsCErbxZhayl41Ec7UhgArmdKf6BMo2Vpa7cERTQvoyyrnehFtzNQHteywwCd/NA4MMoKZCgtKRAnCZ3LN8EFgUtvV3282doNEJXWdSldEwcHAWMdX/PR7FBo8wxYStnVKj7KIpvVaJYUTW5wvYSKlqdWuRaW5FobQAeQ1bZ/1BAfVmemrMQjeUcBbwftQU6lI4dfkslSEZcCVzv5v8EwK+pnoiGTX2W4q7lu3Oysmp5dAihlTef8huKZuTBhK38eQTF0SYha+1IZFEFLESWViFh24hu6heRKzYTddMoblIYCcRBwHFI0KbSM5nYVhTz+i7Z9JPBm35NNcA0NATrvRQnnKuAW2PvjUPiG+zrg1aYOzAwYSt3sukmvMzdyN0DOB4vc1VkSrkNSKg+Hnm9EWUG56CC1qdQucRSYF2n7mUUdcMYCxyDimAPIbe19uayDPg98AsinUxcjdqRqFvuIRRvDT5Hx8LbSWiMKEhEZxa5LqOfY8I2MKgDTkXZwANQG6MFoI66fk31v9AsTm8hq2w+UJ+oa2greUteJrAKj0c1dDuVvI7CtAPPojGiM6OlF35N9RDgBNTOqNixowH30LF55DFo5AZum/N7+FiMPsKEbWAwGwW9DwBGA8fhZX4XsdoWoeFFLUVZY1HUXWMCci9PRKURk4Ft2LzYWZwglvYH4HfA65EEAW57M1Br8O1KXHcz8PecY/cyo1BSBWSt/ZOte6LpAYUJ28DgTWAWErahQA3KIK4EcAH/4otOVds1EVlkh6HRAXuyeWUahWhHIxSuAu4jm45bkpOA7wOfoXu1b48m6hoWxN47AjWtBFmyD+fZrlGmmLANBLLpVrzMDWjweBUqgt2bjoO98+NlBqNJYSajmq6T0NjTsUhIetIyi9KGsra/QkK8OmJlBiMJjkZzhB5E9+J3zcBfY8c7DMUFx7h3nqH/t3kySsCEbeDwNJo8+CAUa/sEhYRNmcxpyHI5GBX57s7m15sVSxPwZyRqcwK3M8CvqR6KLLTvo5ZM3eXVPOdhHIoRBsd5O9n0QJzVa6vFhG3g0ISyiEFlfw1e5hcovpZAA70noFq3/VACIIGugZ7KZBbDBuB+lMyYCbRGRc3F06ageUM9Nt/9vY7ciW5ACYigm8dClFgwBhAmbAOFbLodL/MQyobujoTsahQQn4jiVAl6L07WFa0oyfFL1GRyRR5BG4RGAnzX/b+5vIGSBtFW46MIG0+2I1Fb1UfnxOglTNgGFi+jIU+7IwE7rq93CInHLOBG4Fqy6eZOltsR+CIa89kT4ruRWA2ca5V0AnK9cZ/dYG7owMNmqRpIyAL6M/3DAmlB1uP3UI3dr/OJml9TPcSvqX4v6t12IT1nUc4FamO1eiPQDFij3etHkOgaAwyz2AYe81AWcAZ98+DSnAHKcv6JbPrNfAs513MSmnhlBmGhbE/twzWJuob62PvvI2zMuQ71cjNrbQBiwjbwaEJun4cGxG8pGlHl/u/RSIg38zVsdII2CtXGfRt1/h3Sw/vyFHBnzjteZjs0WXIFYWzt+S14fowtiAnbQEMdP55EBa9nbYEttqDhSL8H7iCbXtLZgn5NdQXKyF6AhHebXtif9cC1RGNr4gNITEHW2g2o/5oxANkS9UpGX+Bl9kfitn0vrD3o0TYb+C1qb+RHi2uj+DXVlai84itoGNMO9M61147c8HMSdQ0bIudiBPAAGjQP6hjyUXNDBy5msQ1c5iKr5Ov07O/cgCY5vg14imy6sdDCfk31cOBsNL/Bfl2vfrOYB1weE7UK4FOEorYG+KWJ2sDGhG3g0oTKHY4nbM3THYLB6bPRnKB3oDGoLfHRAgEujjbabfsCNDyr2DlCu8tqNOfBS7H33w1c7P5uA/6ECoSNAYy5ogMdL3MW8BtKf4i1o4libkfzGzxPNr2i0BecoIGE7GI0GL834mj5uB6YEZuEuQK5pqeia30J8B6yaZteb4BjFtvA5xYUOD+Zrh9kjaj90cNAFnio2I4XrgHkdDT93Wfo3pR63WUm8L08M8ufBnzMHXcj6sRrorYVYMI28FmJJjs5lnDSkoAWVKG/AM1tcB+KU70RzADVFX5N9WA0BvUsJJ7T2bKewHrgUtyEyRGmoGRFUEryMJrz1NgKMFd0a8HLXIgEIOhnNhe5b/8B5pY6SbCb8u5AZJ2dhMRtS9MIXJGoa/h+7FiHIff78+6dDcCRZNPPlLZ6o1wxi23r4c+oaWQwAHwo8BjZ9AvFrsDVoY1FrZFmoEzj5rQU2hxaUe3cL3PeVW+5MwnneGhyyzzbR/tp9AFmsW1NeJld0QTHgXX1OHAK2fSirr7q11RPQEW1n0SFrj09WqAUWlEM8DM5pR06xmNRwiA4xn8C55JNL+vD/TW2MCZsWxNq+X0O6nW2LRKIvwBfJ5teGV3UZTiHoljVySizOBUY3sdH0Y5KTs4DFr4zj4GObTc0mP4gt+wrwIfJpm2Slq0M6+6xNaG6sxtR80VQg8lPAee4LGKc/YC/Az9EDSz7WtRA40C/lqhrWBibmGaU28/93etlaNb41/t6h40tjwnb1kY2vR4JQB2y2AajItpPupncozyPLLr1fb3bjlnAF3BTC76DmkdegmbRqkTNNa9EY1fbS9uEMRCw5MHWyWpkzeyMWoWPR2L3KvAkaD5SoMmvqf41miPgq/StxbYQuChR1zA7511Zmucg17QKuap3A78rNdNrDBzMYtsakUs6B43hfMu9OwX4K17maFexH7ARteq+FJVN9AWzkcucO1O7yjrORrPCB80j/4U68a4sYf3GAMOSB1szmqnq08BP0STE7Wis5YnAa7Gp8EYhK+98NAvWliBoK/5V4PHYhMcVbt+vRBnQdhR/O4Nsek4fnlWjH2DCZoCX+QLwE3Ln2fwC2XRO7ZerYzsbTYk3sZf3ahNqFnluoq5haWx/q4CPoEH5gXv8PHAq2fTLfXYejX6DuaIGqO7rCsIkwf7Ab/Ey1VG31M329EdUnLsIWUm9QRPwO+C8PKJWierpriIUtXmoZMVEzQBM2AzA9VT7CarQb0TXxcFo3oKDoosm6hqaE3UNt6MB5r1Rzb8S+BFwfqKu4a2cT7zMUOB0NBRsJ/fuEpQ8eLCvT6PRf9iSE+Ua/Zl5D7SzxzGzUAnIwagMpBo4gj2OeZ49jlnMvAfe6fRx8W6jF6KutNOByWx+hr0d8FEM7/pEXUPujFZeZiQa1H4pocssSw3+bWUdRhSz2IyQbHoNKvv4CbAWxWCno068NdFFE3UNbYm6hlfREKsfI/dxc5iNBq3fnKhryO3Kqzq1b6Ps7Bj37qtoKr1aEzUjjiUPjI6ojOJ0JFjBtHhLgG8BN8bbavs11UNRJvVbqC6uFFYil/cyoCFnHlANk9oBWWmfRuNT24GH0Bykz3TWxdfYujFhM/KjwtcvIStpnHs3iH9dHZ/rwGVMD0LlF0dSnDfwCsqw3t7BStM+TENxv2OQa9yO+qqdTTb9al+fIqP/YsJmdI4ykEej3mbT0PXSjIZjXQK8GrWY3MD5bZEgnkvns1GtQmNQfwAsdtnW6HYHIxf3u2h0RAUq/7gZ+AawxCw1oxAmbEZhZLkdDPwKWWTBNfMIKth9LB7jcl11j0Du5SGR77SjWa4uBuoSdQ3r8mxvNBLFSwjd4EZU/nEZ2fTbGEYXmLAZxeFlJqAW46cQ9mLbgLKSN5FNr41/xa+p3ga1DL8ABf2vB36cqGtoyLP+QcAeSEDfRzhj+3wUu/u7WWlGsZiwGcWhQP544CJUoBvMPrUGBf/TwLL45C9+TXUVst7GAv/O0xgS1PftBLeOfdF12YqGU30FeNYGtBulYMJmlIaGMx2PBp5Pj3wyByUWbiGbbi5hfXsjq+9ThJZgM3A18GOy6aVFr8swHCZsRulomNV+yML6KLK4QFnTvyGX9fWCU/d5mW3R/AsXIhc0yKK+gSZ6/j3ZdF91EzHKHBM2o/t4meEoS3kukIh8MgclDm7tYL2FongRmnAlGLHQhKb/+zbwnBXdGpuDCZuxeWj85oHINT2YcHq/tWgilZ8DL5FNt+FltkfzJ1wCTEJWWivwpvv+P4FVliQwNhcTNmPzUQJgApr27iLCiZnbgddQ55CFqDzkPYSxtBaUeLgCmFPsrPOG0RUmbEbPocTC/sB30MzzQVuhJmSZjXCvW4EXUbLhTmC9WWlGT2LCZvQ8XmY8Gtt5CR0bUjajWbKuIZt+sa931RiYWHcPo+fR5MRXoVEHv0FDqJqB21Dx7XnIYjOMXsEsNqN3Ueb0g2jkwR3xiZkNwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAMY8tiQ6oKkcqMJGyECOpSsZHaIjtRpDJDCfuTAbQBG6hNtxb5fdDcAtExvY3URpo3pjJDCLtolEKr+9cEtBV9TIZRBlRt/ioGNN8EPuL+bgduRb3Dip1YZAbw2cjrVajj7JNFfr8KuJ1wwuJGNCv6vyLLpNw6S2lo0IImYfGB2cATpDKPU2utuI2BgQlbYZagNtaBZbsMzW/pd/nNVGYwEp39I++2oY4XxQrbTqgbRiBaS4BFsWXGu210t1PLKcBy4BlSme8Az1BbJjNCpTKVaO7R4DreCKyltszbistSHwGMcu+0AaupTW/q610rF6xtUWGeQ2IWsAewXZHf3RWYEntvEPD+ErZ/GLm/0SJgbg8f4yA0X8FxwC2ARypTLiGK8cBPUbPKO9H8pUM3a439By9yXDcAe/X1DpUTZrEV5g00YW8wUclkJFbzCn5LT9wD0Y0X5yhSmSE5cbL86xiE2mhHmVnEU/t/wP0FPq9EcbspwM7uXxCj2xH1T5uPRL2/MwTYG3iXe/0KA+dhvT2aQwJkqY/u6x0qJ0zYCrMUeBk41L0ehG6if3fxvcFI2Ea61+2E7uxY5Dp25Y6OAXaPvG4HHi5in/9LbfpbXS4lq6waOBHNtB6I9wTg66QyX7SYm1GuDJSnW++gWNN9sXePLMJVGwkcGXn9Mpq1KeBDzqorRJJcV/ZNt56eOrZ2atML0cTEX0fxqfAYYc8e25ZhbGFM2Lrmv8haCtgVWVOFGIdcpIAHgdcjrw8iDAx3RjWwQ+T1XIpJWpRKbboNeAB4IfJu4HIbRllirmjXLEI3/b7u9ViUKX2owHeOIYxbbQL+gurZ9kUu6XQU23qhwDoOJayBawNmoZnWe4OVwEvAu93rweSKahA3HEMoyM3AMmrTbc6C3Ru52ElgAbXpv+bdkpadgCzCqcgFHopE+y00F8KCgrHEVGZ7dO1OIpzKD5RJnEwq04hKWpZ3sZ6RwDRgN7dP44F16Dd/BXiJ2nRjyWczlRnl1rmnW+9YYL07vtfcMa7pUDuo/QmmLtw28kklkCCVqXav11CbXlPyfm1FmLB1TRuy2gJhGwnsSSrzUN6iVt24UTf0VWStPQl8BolGkkLCJhE5OPLORmBWL5YxtCEBjjIk9roKOAM4zb1+HjifVKYdOB34GrLyBiOR7Chsqcw2KNt3LhK1CbElGlHiYiapTAZY4izKONegRMcQYJfI+0cDf3fHswi40J3/+H4MRrHSCwjFeHBkiU3u+zNJZX4MzO1kPzpb70WR9VZGlmhBcdtngKtJZR4AWiLX0bHAt93f0XMzBsgg0QX4A0ryGJ1grmjXtAGPEMaghiKLrbOygsmEIggSxdUoVhesYwiFyz5GEGb6QPG5x3rxGIfQMeu2PPa6AonJQe7fHsgqvRLNSDWNXHHIJZVJolmq/oTKWCbkWWo4svzOBR4FPt5JLHJvtw/7ECZoQKU4B0Q+6zgiI5UZjZIl9wMfQwIb3+/BSKRPR276aU60OieVGYGmG7wPJWR2JlfUQA+HycBHUbH392L7mIic3x1j+7N75LPJGAUxYesKPU3nAYsj7+6FSiZy0U24B3KRQEL2FHLbFiA3JOAoV9KRj30JJxcGjQ5Y0YtHOdbtd8BaZFkUohJZoJ8jFIZWZHXlum+pzG6osPlYwpt9tTs3fwZ+C/wHuWqBZTQF1ah9zBXiRvFRCcTb5FqajW6/l7hlWmL7MRz4LrLUAkFpRrPUPwrUuf8Xu/dBAvwzINVp0khD576GLLXgd9uIkj3/AK5FmfQ3I+sdhZI2MyLHt8Ht+xI0MiSgDf3+wWdrMQpirmhxvIouyp3d671RDGRZbLkKVOYRDIFaATzpXMhWUpn/IIsClITYhVyxCziU3N/mvl4ey3k8uQWgbyKXsBBT0c08DI03vQe524uIJjlk6VyKCoBBiZj/Idfq3nfq+STyuyBX7BQkPDug2eLjdXUfJ4wDXoOsM5B4pJHABW5fsB8VyEI7n1BclwK/RDHQhdSmg4fTFOATwMXICky4Y3gRudlx3oestVGR9V4BXP9OLEzr3R44C/iqW+9wtz+PkMrMQsW4j7t1nIEsS5D1fB5hidCqHvvlBygmbMWxDngCxXBAMY896XjzVwHvJbSEF5I7UmAmukBHIPfvSFKZ13JES0//AwhvvrUUPwSreCQk45DgpMl1m+5FxcmFGI9Eaj7wHeAuFNRuj2yjArllNZHvzUaW3us5yyqG9RqpzPnogfE19KDYFbiYVOb0d4Z61aYb3PpbkKhGf6cFndTf7YEsquA4feTy3pmTYNBvsYBU5mdumSuRRbsrcC6pzNdjTQjGkytqjUgQb86z3iUuZrfJnbPhhLWEz1CbXgusdSIYtdBbgcXUprv6TQyHCVsx6En+IBpsHnAocl2iDCd3tMDM2AiD11AiYW9kcRyKguzRZcajmyhgPlBfwt5OJ5X5dIHPB6G41BRkXR5ObpxnEfDrIscltiCL4+5OEhvbomRD4J6tBL5Hbbpza7A2vYpUJrDw9kLidgwKxj9VwnmIcxzKRgf7/Sfgrk6Psza9iVTmJjTZ8yeQIJ4CXA4EwgpwBOF44HYgC9xaYL1NpDLXuHXu736PDwDfRwJm9AAWYyueZ8l1PQ8jlYk/GA4hTNe3oLhRlHpgjvu7Al3YE2PLTEKB+IAXUeypWD4IXFfg37XAL5D1ciy5ovY68GVya+4KcRvw7wLZ2qnkZogfRsH4rlgN/IpQ8McBHy4QkyyMBOizhA/yt4Bfk2vtdaQ2vRG4njDuNwGJUEAVEsygNGM1cBMq7Si03tXuXLS4f1PpmGgwNgMTtuJZT67FMJl4rVfoqoLiVLkCoRvl0cg7u6O4S5SphGNMVb+m7xVLFYp7Ffo3hNxefO2ofOMc5JoVU1bShNzPQp1A4k0D7i+q/krbf5LAMtJNvx+5GdBSGEtuwfQL1KbfLDJuOQ8F7AMOifxdSVj7B4qFPVHkeq9FcbQzgK/QsdzG2AzMFS2eRhQj+yB6IIxHro3cxFRmLCpjCHgSZe3i1KHyiEHoSX84Kr4NeC+h6LTR0errirWEweV2wlET0f/b3PGsRe5xFvV421BCkmI5ipMVWuZdsdePUzxvIEEJ6tSmozhWdzKCB5N7rb9AKrNDkd/dFiUDguX3IJUZ4eJ4g8kt7akn/2/ekdr0XHq+U4vhMGErFlXYz0Huxlh0wU8jlQlcsSmofxrIinmW/C5JkHEM3M2jSGV+4eJ4FeRaAEvIV2BamDuQcEJhYVPvMlhadEffXBoJC0Y7IxF73UDxrCZXxEZTqE6uMJNir08h17ouRBWyogOGo5jhBiS00X1abJ2I+wcmbKXxLHp6j0Xn7l3IrWtCrk7SLbcOeCivS1ebbieVqUNZP9w6EigDFx/4fm83mj4upjY9q8TvdIcWunaf4r3rih8GpPMUFc5t6H4camzs9VRyxaoUhhAmQ8Z0+/iMXsWErTQWIwsqKGYNRiA0oexYcD59wiRBPh4Hvoie/tugavJ7kFsTBPNb0aiFciYeGxxCvHi3MxTwj1pDzeQ2IyiFuCCuybNvxbKSMHsZTz7Y/dRPsB+iFGrTraQy96MhMaBatnHOsohmyx7qYvD0HOSS7oHcmUNJZe5FAhcI22KUES1n4gXM45GLWQxDyR3mtZbi55qIE9/m98g3lrU4WghjmCvJ7bVnzSD7CSZspfNfZD0Mcf8OQpnGoPNCK4U72IIC44HlV+nWsQMKkFdGlim27KK/Eh9VsTddj2gImESuK9tA962seEB/LLXp4oL8hWlCoYkgsz2FVKayqJhlKrMTYZy1DdU8lsdcE2WACVvpLEIlAMEwnmBAd2Xk85cKrqE2vZFU5iFCy+9AFPOZHllqNh0tnnLjKRSHC1zKo0ll6rq88cMxt9WRd2fT/RjWs7HXhxT9zVRmGIqDBk0P3qA2HTxwWoCnCWcym4gSSMU8kNJoeBWorm5Xum+RGjGsjq10VpNbnnEwqowPzuUcisv+/YcwVjMJjTeMjji4v+xnW9INHnWnjyW33XlnVKLK/CDovw51WGku4rv5aCDXetyXVGZ6kd89HJXD3OL+7R/5rBWVAAWCNB5luQt3WE5lJqKynoAnMVHrUUzYSqcRWQDBTbYHYe+0NvfZqiLWs4DwZqtAA7sDq6CJ0mq++isNwN2ElfvTgbOKaIt+OBK2QCDeQCMcurcXGod6M2HyYTxwuhuX2zlqcXQOconHuON4KrbeBwinRBwJfIrcJpHxdQ5GoyCC0qBm9JAzYetBTNhKRTfXE4TiNY6wP5aGURVnaW1Eg80DopbMLHq3TdGWQTGj3xDW4g1C2eA0qcy4DsunMlWkMik09CtIoqwDfkL+7sFN5GYmg461+fg7Yaa6ClX7n0cqs10HoU1lKkhldgZ+juaGBf2215Hbvgr0ILsl8vp9wI9IZbbPs96RqIbu/wi7I88h/7C0DYRity2wT7eHlG2FWIyte7yIKu8nkDs0aS2KuRRDCxKwRnQTR9fzGANlQHRtepHrhns1yhoOQzf2oaQyN6PY2UbUEurDwCcJx8+2oYaM2U6stbXkFgnvB/zGNSx4HXiA2nQgiHNRf7c/onM9HGVHjwJqSWWeQ2GGSShumkKWeBA7fRm1Icqt3VOm/GrU+mk6Eu/Po9bgN5LKPIPijNNQTPUkQouuHYnlG7F1Qiqz0p2XUW5fL0AF4XOAp6lNx+OGRgR7AnQHDafJNxXeA9Sm1xe5DpBbEx/gvhZ4vJujAforNwFfILRCR6BWRn9F7dFfRe7YVwlFrQm4EfhSp+NLVVITddmrkMD8GFl5O0aWbXPb+wah9TfS7cfv0W/xKoqZXYHErRKJz4vAGdSmOxsFsgB12w3EZijqkPwH1EfuRaAWOJMwbrgBuAz4YycW/ovkNvvcGXVSuQ44ecv8bOWLCVv3mYmexNF/D5a4jvnoaR1dRwOaSKRY2ghHAWwijGf1NGqWGW6nhWILZiXSt6Ib+1EkWp19dxMqcP42cGERD4o/oCx0tIA32Nfcbcg1vhq5oXO72I92NCTuTuDTFLLE9ZB6GsXj7kGi1dl6W9BQue8AV1Kb7qzDyMtI2NeR+5u2MVCs+V7EXNHucx8aFhW4Kpvo2J+tMOrN9UN08wQsIX9X3c54DE1aEriyz/TS8bahcahBb7iVdN0+PHqsbcjluwdN6HIUSryMQeUgq1H86hGgLlJS0dV6F5LKHIWaNU5HIznWIOtrcZ7lG5GLeBdwAspOTkXu4RAkSovRw6UWdUDuOhsrcZtFKnMCcqmPdfsz1q13HSrKnoXifQsLJkPk4v4QJSfej+ocNfOWxNMoQFcT/xpG75DKDEGiNhx5DhvRzb+229lPlVlUoZmfurYmFdwfhkRtGHpIbULhgLWbFQ5Q9nOsW28Vsg5XA+tLLuPRflYB7QMsRGEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhenVDrq+lwy6krYDGxPZ+qL78ftechDqPloBNCey9ZvyrLctka1v7IsTt5nnJega2wo0JbL17b6XrEAtpKtQG+7GRLZ+M7aSs73Bbt0AGxLZ+i0yEbPvJSsJp5nbmMjWF9X91feSoIlWQB1tm3vqXBhbB709mcsP0Szavwe2L/G7uwB/dt8/KfbZpe79H22Bc9QbXOH2/5toxiaQAFzo3r+aUIh6ghOA29y6S/0dNocD3DZvAg4s4XtVwD/dd8/CWtgbJdLbk7kcBByNJgAZXuJ3twGORNOx/Sf22QHAMYQzp5cb7wYORb31g8lgKoG93XG9RM8+dJJoQpAqQiHFWYkHovPZCNzWwxbwOHc8q4FrSvheBZp4eBiahs4wSqI/z1LVim6IoWiij4FOMN3bKiR4PekuBhOJVJI7lVsFmjHq22jKu/uQwPUHVqPfvb/sj1FG9GdhexlNYVZJOMHtQGYDck1/iObI7HrKt+K5EbgLCVlDXx9oEbQAh7j9XUPPiryxFdBvhS2RrW8GFvb1fmzB421HVpPfC+tegwSiLHDn4s2+3g+jfOm3wuZ7yVEo5jQUmJ/I1hdlabiM2lhgTzQRbyMwO5Gt3xhbbgdgf+BwYAKykOYC/wXmJLL1LZux72NQDO0wNNHtSuAp4OFEtn5pJ98ZBOyGYorrgGfjWWTfS1YBuwNHuHMzGM0kf4/b97FuHc3umBsjxzoVxe3+5z7fBdgR2MmtfjBwmO8lVwIvR/fTbXc3FDPd321nDbAIeBZ4OpGt7zGr2sX+3oOs9UWJbP18934lMM2do2UoFjkexQ8PRpnUBcAc4L+JbP2qTtY/GNjXHU8wWfIidw6fSGTrF+b5zo7unDUCLyWy9evyLDMU2MftxyJ03ba7z7ZznzUDs1FI4D3Ah9y5v9g9zKPXQhD/3A5Z9G+6fXwMWBnPFPteMuGuj0rgcWTpHuaulylosuV57vuvdpYdd+dnOrqGg0molwPPoQmtGzqrcHAZ/wNRHHl3pDEL3TZnAau3RIa73wobsDNwAxKGS1CmsBiGIHfuVHTBXIUu9HfwveTBwJXAu8hNarShRMfVvpf8dVwMi8H3kkngB8DJhCULoLKFu3wv+Y1OvhpkRU8FXkBJl6bIegcBnwW+QShSAecAP0Zxqd+gmcxrgGA29Y8Cl7vzsQeabf5s4EvuPdDN/Vd0M5wF3Oy2WwWcCZyPLtR4UmM18JDvJb+SyNb31BVbBdzqzt/VyEUHPeQuBj6OZqX/IcqQfzRyHCARuNX3kt9JZOsXxH6fUcAFwBlI2KMZ1ybgWd9Lfge4L3bjf8xtbwHwKeD5PPs9zp3/6ShZ8k3kVoNu9FuRRX6C2+evu++sB/4PaHbifZr77jQ63qMbgLuB7yJhj3IE8Ft33vZB1+DFSPwD2tHD6Ae+l6yLl+A4cb7A/eY7x85PG3owXg7cGT9430tOdMfxSSAR+3gNCodcgu6xXqU/C9sglMEbSe5F2ynuiXUZ+lEagQzwi0S2fkNkmZOAX6Oyh7XAw+hGH4EupN3dOnbyveTXS7HcfC85HvgHig+1oifzm+7vHZHYTC1wPEPd8Q6PrbcK+CrwPff52+jJvRzdGHsjAb8XGOWOJXpBDnbvV7nz2o4urqfQk3wndAM+hwR4udtuBfB5dLO2oRtiJrJGJiCL5yh0ow7yveQZiWz9sp748d0xjKJj5js4RzuhcpCd3H69RSjcU4FPA0N8L/m54AHlzmMaCcom99vPQ2KxAxKGQ4E/IOG7N7LdIZHfprOMdYX7fCQdy3Uq3ftNwJfd/o1AD4YGoM2d788BP3fLznb72ACMcfv3biRYk30veVwiW782so0q972RSIQ/7n7nZ5AXsAOypg4ErgU+gB6iuPMTfO98d43MQwLuI5F7N7Iyb/a95IlExN/3kpPRw/BId25fBF5xfyeRhfxJYJrvJU8NrPDeoj8LW9E493MYqg/7JLp4rgKuiolaAllT2yOr5lL0FF2BLsTdUIbQQxf2bb6XfKgY09lZVGchUduErM0rkbC1u21+C1ldpdZl7YKsuZFI0C5EorQKXfDHIhE/vsh1t6IawVuAi9ATeo3b/2WE8biE299ByAU5A1iQyNYHN2EC+CmyYN6HHgw9JWxd8W53nn+Jfuu30fW8L/AzJFAeErmgZGQkeuhVIMvmJ4HL7dyvD7rv7gac43vJe3vBbdoWWeUb0PXxIHqQNCPxOxu5fo8gAXwhIh7bI2H+EnqovMt9Px8nowfV2chy34Bc+POB89zfn0YeQHAPfQj93rjtnw/MTWTrm52l+x7g7+iaOx89UJa5gvmvILe3FT0YrkJhkhZ07Z+KrMz9gS/7XvKSRLa+iV5iQAgbsh4uR0+7JmTG/zbqSjrh+QJ6orejH+KWyIXbAjzne8kvAnshy+18FBdYX8Q+TEWiCnJ9vxxzZRf4XvJC5BZ4xR6Yu+DOQRfiahSLuTuyyNvATb6XXIvcsy5xx7zO95Ib0AUPssiWJ7L1b0cW3RPYFQnIDxPZ+tcj62gH3va95O/Qk38iErbHu/8zlsRg4E/AtyLnucn3ko8jsQtiXfsRCtsuSIxXoJq9d+KIbmTLXb6XnAp8B10nw+n5cpNKdPOflsjWRy3CQLgOdtu8OpGtnx39PJGtX+J7yR8DX3Tr2ZfOhe1N4MzYOhp8L3k58GHkLh/ke8kq55VUAl9D8dMFwIxEtn5uZNvrgP/4XvIK5NEchqy4ZeheOdOt4xbg/JhoNfhe8lfAJHRPfR49WOb18Ll9h94eedDbVPheclv05DsNmduXERM1x3YoblWJrJ478z2NE9l6nzB+sBcSrGKYhn5ogL91Ep9bhVzVUhiDLnaQW/FwJ8s9BLzWg+cWJAinoSf5E/EPI0OfgiLj8UWvuWf4Xfw8u9/0ZUJBmhL5OAh4j0CJkjHugRflL0ioP4kEvadpRy70o3k+WwMch+Jv8aL0IDSwjXtZga7pzriXjjE4Etn65YQZ523dP9CD6WC3f/9G1lY+HkHu6xoUKgBZ6+PQA+MGIrHhCE0oxrbSbfO9vXBu36HcLbZxwK/QzVcJPAlc14mojEfiA7IqqlzMJR9BsmESMBnFOrpiF/RDtyMXoAOJbD2+l8yiG6zYh8oOhIHY+bGYSpRG4AFkYfUILl72QPDa95JD0A0w0Z2bHdDTd5xbZEsOfVpP57/LSsKgfTTWtQCYjx5WlyHBfsj3kk8jEW9w2d3erJvcBMyKhkgCAqsoeO3c44nIlZuIRPpEwvHXhc73rALx4aCkqJJQAw5AVnAzynLnTZwlsvWPIis4ynsi630NGOkeenEa0O82HrnR1/XWSS53YZuGLtI29CMfiNy8a/MsO4LwCZdCgdjOGBn5f3SR+zLW/b8BJSXyksjWr/e95AqKt26GEwbQlxRYrq2Lz7uF7yVHoIBwDbqAx7p9GkaY5OjqJusNFpI7iiJ+LvKVMqxFMaVLURztWGRtrEHW9FzfS/4LWRYLN6fkpwDNdPI7OTEYgSynk1CccDw618PcZ8G13lXRcqE6wHznbZL7v4USriO3z9Xu5c7I2+ms2UFlZDuT6EXKXdgqkYl7DRK0XYFv+l7y7jy1SIMJb8RxhFZGISoofoxr8GNuovMfNmAJxQtbO+FFvLGL5Xo0cO97ybEoPf8lQrFvQjfNc+jp/DYwg16+UPNQTNwzB1d7davvJeei6+UYZDmMdf92RvGn81Ai6g8lbqKCrgW+jc7jdpUoCfRZwgflJmRpvohc7OfQg7ur7ZR6fgLLtp0w7loMlYTjj4dSvMcwssjlukW5C9sKlNW7CbkYv0Kp5e/5XvKrMbetFT0th6DA5RVFbqNYsViBLoptCGMPnVFKQ4D1hIK2QxfL9lhTABfP+Sy6yYegmNB1qIB5FTqXTSgI/bme2u6WIJGtf8n3ki+jsp9tkGt1OLLe9kTW3OW+l3w+ka1/uoRVDyEUpJJw5/tTKBM6CMU0r0ex0xWE5xv0IK8sfSsFCdzvSjrWoMX3cxAS1lbC+woU0/s0xbnyvToGuNyF7RXghkS2vsX3kjegcoR3oVT33aiUI2ADukBGAePjhZs9QD0SoVEoLpcXF9erLnalyLoLLpQdfS85qJOq70ElrrcrBqEU/XCU1j8zka3vkMVycaB+fx05l2k7JD4b3aiEYKjZIuBuN2LEQ9nWsWhEQ1zYKulcVEbQ/QTKcOAUdC7r0blfkGd0QbGhkVIJ6sqqKGx974HEdyTw00S2/gXfSy5C5TdDUe+8Bb20j0VT7lnRliAOksjWr0dlHktRXOwyNwwmYClhluho52blxfeSl/pecpnvJWf6XnKXIvdlHmGl/+nuho+vF1TBXopltZowg7YbnccGx6JsWk8xCKXxISw0zsd0Cjzh+xEVyEJ7A4lYBzF2Yvc3lF2vJFekAst9FHmsMvfbnkz3++iNQ2VLILczn6iB4p29cd++QFgLeIwbApYPD4Uejo3sR1ByMgk41Fl1HfC95FG+l3zF3Vs9ea12YEsKW6XvJauK+Lc5+/QooZW2C3CeKx4EWT33o+DoeOBcFxh/B99LVvhecj/kEoxDT7FFRW77TZQKBwV/j4sei7sod0IXRdG4i/sfyA0ZC3ze95Kjo1knd4xnuWMulcD6G+x+o+D9dsIkyGRiSRR3rnZCoYBSe+1tcVzd3TwUhN8DODJ+A7rXeyNrpIXc336BOyfbAu+K/bYVyIX9xGbsYhOhqzkFGBb7jStQsuxiIhZjJ9nH7m7/FneMRwBHRM+P+72rUcFvBYr1BXHsB5DhMALVim4f+y6uLOtsdI1upJPKgZ5iS7kQE1Agtpig5DXA7d3ZSCJb3+h7ye+jCvydkYg8gmrW2nwveS0a/nMk8H1gf99L3oTcvcFIkL6MBGgJcEWx1dHOHf4hql6fhuqhrvS95GPoh9wJVY3vjVzi7YpZr+N54BeogPJzSHRv8L3kYree05AlWFJ20p2TFegmHoNGXdzne8kn0BClB4DTUf3fRb6X/BuKjYxz7wVV8uuQJXOg7yX3QQOs+2MPvb+hmOF26Hr8tSv1WIus6P1RAWkFErVs5LuvozKgfVBmdYPvJZ9BD4bd0RjJBBrR0p1EynLk8h+JAvBXAn/xveQa9CB+NxpWNwrdR8NRneV+vpecn29Qfim4a+EqlIk9EI1MudJdC+vdts5H99U6NFRxhfv6PLe/l7rv/wf4je8lX0KCOQWVBX0QXT8/deep19hSwjYMBWeLoW4zt7UMDYu5Al0E33A/ztuJbP1a30t+DY3FOwy5Dscjay5wPSqRu/JtFMMrhaVIfH6GLs7vEw6X2Q6d7zvRhXJqCevd5PZ5Z+QK1KCM3ip3jKPctmejAs9S+pcFYwEnoREOn0FP3b+hh8xBhBf1ye5YtkHitgxVwX8CldCc6JY9mZ4vFu4JXkVZx0vQg+ZH6CHTiB5s493/i9C43Kj73YCykRlktV1BmDAKum/8CLloJQtbIlvf6nvJn6Pfdgp6aJzg1jsaWetLkIV8HOG1OxWJxrM9dH4uQqK/I7p+VyBxGofEdCUa2fFQZN83+V7yOvedMwnHLi9H1+5YZAVvQNME/LGXSmneobeF7UHkt5fCq+7/VUjkxtJRYB4mbMGSg3vy3Ijcp13Qid0fVWK3J7L1T/teMoVKGI5C1lUwOPkZVOR7bSJbX3JLaufu3OV7yVfd+g93+1GFnva3okzXSSgMMIuwkLQVZcKGoBuqLbJegKW+lzwDZZ1ORO5UMCD+FpSxPMN9ZUNkvSCR+ScS7bjV/F9kbaTQxbsOeMtt8wnfS56KBo0fhApFh6Cb/C8oyD4XlSEMQTfkEkKXaglyozdQWo1dG7Lah5HrsrSi7hJV6OHTWR3bBvQAGUOktbgTj1+4dZ6OrK+JSKg3oTjTsygW93w0xuUs8muQJfsZZKWNd+frbtQo4Gl3jgPrK/qAWezORWOBc7EAxUnPRe7gRCSiC93vd10iWz/H95JPueX3Qg+XoLSjAc1tMRQJUmc8idzGBURKiNykQg+iwuVzkPW4g1vfG+63/h3wQFyYEtn61a5zzf3u3Aax10qUDJmDBsnfHbRn6k16W9h+0o1tBCd6IRqcPYiO9VtXuvXmrRdLZOtX+V7yB5FtNxO5yBLZ+hW+l7wMlX2MQU/pVuSSLGPzu9e+gmIhCcLRCOuApe4G+QuyiFoix9aEnmZ/dvsSnZUrSK83IgG7NbLfTcha24RcAdCNFT1n9yMBq3D7ET1XrS6jXIuEpJncAuMX0bjacehmCI7l7WDmMN9LPodu9tFuH5dFvvsF951S0vut6MFQEfstmtzx/8kt05mwLUdiPIjY8B734HsACeR27piCmcHWofGyeffVfbcWPbDHIAum2X1nlYsr/cytr5nc6/OFyLnorKof4CXfS16Erp0gBhxcl8GxzEYW3Ri3rmAkwSy3nQoKh33+iLoqt8Z/F/dwftn3kpe4fRjpzmMjEss1nTUGcOGHO3wv+ZA7t0HstRFY4RqebhFs9p8ywPeSp6D430rgl500OZyGOn6MRhfuuf00zmUYvU65l3tsLYxGrYouAU50zQjfwfeSk1BiYjR6Ut/L1jEBjmHkpd8XVhqAhGoBimFdCiR8L3kfejBNQ6UeR7tln0JxjL7eZ8PoM8wVLQNc7OZDKGY5HcXmgthTFQrubkLT513UncSHYQwkTNjKBOd+7otq1t6PMr4jUdztSTShy23AYrPWDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMY4Pw/tPiC5rO1F7kAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTYtMDQtMDJUMDQ6NDY6MTEtMDU6MDCQZFyYAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE1LTA5LTE1VDA3OjMyOjU5LTA1OjAwv4pzxwAAAABJRU5ErkJggg==') #ffffff no-repeat center center;
            background-size: 132px;
        }

        .mwp-notice-middle {
            padding: 21px 5px 19px;
            background: #fff;
        }

        .mwp-notice-middle h3 {
            font-size: 22px;
            line-height: 22px;
            font-weight: bold;
            margin: 0 0 10px;
        }

        .mwp-notice-middle p {
            margin: 10px 0 0;
            font-size: 16px;
            line-height: 24px;
        }

        .mwp-notice-right {
            background: #fff;
            width: 178px;
            padding: 26px;
            vertical-align: middle;
            border-radius: 0 4px 4px 0;
        }

        .mwp-notice-button {
            display: block;
            text-decoration: none !important;
            background: #75B214;
            text-align: center;
            vertical-align: bottom;
            white-space: nowrap;
            outline: 0 none !important;
            overflow: visible;
            box-sizing: border-box;
            color: #FFFFFF !important;
            border: 1px solid transparent;
            cursor: pointer;
            border-radius: 4px;
            margin: 0;
            -webkit-transition: background-color 0.1s, border-color 0.2s, color 0.1s;
            transition: background-color 0.1s, border-color 0.2s, color 0.1s;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            font-size: 16px;
            padding: 8px 16px 10px !important;
            line-height: 22px;
            font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            box-shadow: 0 -2px 0 0 rgba(50, 53, 57, 0.25) inset;
            border-bottom-color: rgba(50, 53, 57, 0.25);
        }
        
        .mwp-notice-button:hover {
            background-color: #609905;
        }

    </style>

    <div class="mwp-notice-container">
        <table class="mwp-notice">
            <tr>
                <td class="mwp-notice-left">
                </td>
                <td class="mwp-notice-middle">
                    <h3>You&#8217;re almost there&hellip;</h3>

                    <p>{$message}</p>
                </td>
                <td class="mwp-notice-right">
                    <a class="mwp-notice-button" target="_blank" href="https://wprotect.cc/#features">Learn more</a>
                </td>
            </tr>
        </table>
    </div>
</div>
HTML;
    }
}

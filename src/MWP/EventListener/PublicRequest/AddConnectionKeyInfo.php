<?php
/*
 * This file is part of the ManageWP Worker plugin.
 *
 * (c) ManageWP LLC <contact@managewp.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class MWP_EventListener_PublicRequest_AddConnectionKeyInfo implements Symfony_EventDispatcher_EventSubscriberInterface
{
    private $context;

    private $slug = 'wprotect-master/init.php';

    function __construct(MWP_WordPress_Context $context)
    {
        $this->context = $context;
    }

    public static function getSubscribedEvents()
    {
        return array(
            MWP_Event_Events::PUBLIC_REQUEST => 'onPublicRequest',
        );
    }

    public function onPublicRequest()
    {
        $this->context->addAction('admin_init', array($this, 'enqueueConnectionModalOpenScripts'));
        $this->context->addAction('admin_init', array($this, 'enqueueConnectionModalOpenStyles'));
        $this->context->addFilter('plugin_row_meta', array($this, 'addConnectionKeyLink'), 10, 2);
        $this->context->addAction('admin_head', array($this, 'printConnectionModalOpenScript'));
        $this->context->addAction('admin_footer', array($this, 'printConnectionModalDialog'));
    }

    public function enqueueConnectionModalOpenScripts()
    {
        $this->context->enqueueScript('jquery');
        $this->context->enqueueScript('jquery-ui-core');
        $this->context->enqueueScript('jquery-ui-dialog');
    }

    public function enqueueConnectionModalOpenStyles()
    {
        $this->context->enqueueStyle('wp-jquery-ui');
        $this->context->enqueueStyle('wp-jquery-ui-dialog');
    }

    public function printConnectionModalOpenScript()
    {
        if (!$this->userCanViewConnectionKey()) {
            return;
        }

        ob_start()
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                var $connectionKeyDialog = $('#mwp_connection_key_dialog');
                $('#mwp-view-connection-key').click(function(e) {
                    e.preventDefault();
                    $connectionKeyDialog.dialog({
                        draggable: false,
                        resizable: false,
                        modal: true,
                        width: '530px',
                        height: 'auto',
                        title: 'Connection Key',
                        close: function() {
                            $(this).dialog("destroy");
                        }
                    });
                });
            });
        </script>
        <?php

        $content = ob_get_clean();
        $this->context->output($content);
    }

    public function printConnectionModalDialog()
    {
        if ($this->context->isMultisite() && !$this->context->isNetworkAdmin()) {
            return;
        }

        if (!$this->userCanViewConnectionKey()) {
            return;
        }

        ob_start();
        ?>
        <div id="mwp_connection_key_dialog" style="display: none;">
            <?php if (!mwp_get_communication_key()) { ?>
            <p>Connecting your website to WProtect services is super simple!</p>

            <h2>Automatic</h2>
            <ol>
                <li>Copy your secret Connection Key below. (Select the key, Right-click and click COPY)</li>
                <li>Visit this page: <a href="https://wprotect.cc/#connect" target="_blank">WProtect Connect a Site</a></li>
                <li>Paste your Connection Key in the form (Right-click and click PASTE)</li>
                <li>That is it. We will send you a confirmation email once your site is active.</li>
            </ol>

            <?php } ?>

            <div style="text-align: center;font-weight: bold;"><p style="margin-bottom: 4px;margin-top: 20px;">Connection Key</p></div>
            <div style="padding: 10px;background-color: #fafafa;border: 1px solid black;border-radius: 10px;font-weight: bold;font-size: 14px;text-align: center;"><?php echo mwp_get_potential_key(); ?></div>
        </div>
        <?php

        $content = ob_get_clean();
        $this->context->output($content);
    }

    /**
     * @wp_filter
     */
    public function addConnectionKeyLink($meta, $slug)
    {
        if ($this->context->isMultisite() && !$this->context->isNetworkAdmin()) {
            return $meta;
        }

        if ($slug !== $this->slug) {
            return $meta;
        }

        if (!$this->userCanViewConnectionKey()) {
            return $meta;
        }

        $meta[] = '<a href="#" id="mwp-view-connection-key" mwp-key="'.mwp_get_potential_key().'">View connection key</a>';

        return $meta;
    }

    private function userCanViewConnectionKey()
    {
        return $this->context->isGranted('activate_plugins');
    }
}
